package com.houxm.caipiao.util;

import java.util.Calendar;
import java.util.Date;

import com.houxm.caipiao.model.Ssq_drawing_reward;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

/**
 * 获取下期期次编号
 * @author 学明
 *
 */
public class NextTermUtil {

	public static void main(String[] args) {
		PropKit.use("a_little_config.txt");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"),
		PropKit.get("user"),
		PropKit.get("password").trim());
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.addMapping("ssq_drawing_reward", Ssq_drawing_reward.class);
		arp.start();
		System.out.println(next());
	}
	
	/**
	 * 获取下一期次编号
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static int next(){
		int nextTerm = 0;
		try {
			String sql = "SELECT * FROM ssq_drawing_reward order by term desc limit 1;";
			Ssq_drawing_reward max = Ssq_drawing_reward.me.findFirst(sql);
			int lastTerm = max.getInt("term");
			Date lastDate = max.getDate("lotteryDate");
			int lastWeek = max.getInt("lotteryWeek");
			Date nextDate = null;
			int offset = 0;
			if(lastWeek==4){
				offset = 3;
			}else if(lastWeek==2 || lastWeek==7){
				offset = 2;
			}else{
				return 0;
			}
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(lastDate);
			cal.add(Calendar.DATE, offset);
			nextDate = cal.getTime();
			if(lastDate.getYear() == nextDate.getYear()){
				nextTerm = lastTerm + 1;
			}else{
				int year = Integer.parseInt(String.valueOf(lastTerm).substring(0, 4));
				int newYear = year+1;
				nextTerm = Integer.parseInt(newYear+"001");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nextTerm;
	}
	
	/**
	 * 获取下2期次的编号
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static int nextNext(){
		int nextTerm = 0;
		try {
			String sql = "SELECT * FROM ssq_drawing_reward order by term desc limit 1;";
			Ssq_drawing_reward max = Ssq_drawing_reward.me.findFirst(sql);
			int lastTerm = max.getInt("term");
			Date lastDate = max.getDate("lotteryDate");
			int lastWeek = max.getInt("lotteryWeek");
			//计算下一期次
			Date nextDate = null;
			int offset = 0;
			if(lastWeek==4){
				offset = 3;
				lastWeek = 7;
			}else if(lastWeek==2){
				offset = 2;
				lastWeek = 4;
			}else if(lastWeek==7){
				offset = 2;
				lastWeek = 2;
			}else{
				return 0;
			}
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(lastDate);
			cal.add(Calendar.DATE, offset);
			nextDate = cal.getTime();
			if(lastDate.getYear() == nextDate.getYear()){
				nextTerm = lastTerm + 1;
			}else{
				int year = Integer.parseInt(String.valueOf(lastTerm).substring(0, 4));
				int newYear = year+1;
				nextTerm = Integer.parseInt(newYear+"001");
			}
			
			//计算再下一期次
			Date nextDate2 = null;
			int offset2 = 0;
			if(lastWeek==4){
				offset2 = 2;
			}else if(lastWeek==2){
				offset2 = 3;
			}else if(lastWeek==7){
				offset2 = 2;
			}
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(nextDate);
			cal2.add(Calendar.DATE, offset2);
			nextDate2 = cal2.getTime();
			if(nextDate.getYear() == nextDate2.getYear()){
				nextTerm = nextTerm + 1;
			}else{
				int year = Integer.parseInt(String.valueOf(lastTerm).substring(0, 4));
				int newYear = year+1;
				nextTerm = Integer.parseInt(newYear+"001");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nextTerm;
	}

}
