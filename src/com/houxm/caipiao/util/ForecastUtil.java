package com.houxm.caipiao.util;

import java.util.List;

import com.houxm.caipiao.model.Ssq_blue_plan_forecast;
import com.houxm.caipiao.model.Ssq_group_plan_forecast;
import com.houxm.caipiao.model.Ssq_red_plan_forecast;

public class ForecastUtil {

	/**
	 * 获得某期次的红球预测号球排序
	 * @param term
	 * @return
	 */
	public static int[] getForecastRed(int term){
		String sql = "select * from ssq_red_plan_forecast where term=?";
		Ssq_red_plan_forecast fore = Ssq_red_plan_forecast.me.findFirst(sql, term);
		int[] red = fore.getReds(33);
		return red;
	}
	
	/**
	 * 获得某期次的红球预测积分值的排序
	 * @param term
	 * @return
	 */
	public static float[] getForecastRedOdds(int term){
		String sql = "select * from ssq_red_plan_forecast where term=?";
		Ssq_red_plan_forecast fore = Ssq_red_plan_forecast.me.findFirst(sql, term);
		float[] odds = fore.getRedOdds(33);
		return odds;
	}
	
	public static int[] getForecastBlue(int term){
		Ssq_blue_plan_forecast bluePlan = Ssq_blue_plan_forecast.me.findFirst("select * from ssq_blue_plan_forecast where term=?", term);
		int[] blues = bluePlan.getBlueInts(16);
		return blues;
	}
	
	public static List<Ssq_group_plan_forecast> getForecastGroup(int term){
		String sql = "select * from ssq_group_plan_forecast where term=?";
		List<Ssq_group_plan_forecast> fore = Ssq_group_plan_forecast.me.find(sql, term);
		return fore;
	}
}
