package com.houxm.caipiao.util;

import java.util.List;

import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_drawing_reward;

/**
 * 获取近期开奖信息
 * @author 学明
 *
 */
public class DrawingHistoryUtil {

	public static String SQL = "select * from Ssq_drawing_history where term=?";
	public static String LAST_SQL = "select * from Ssq_drawing_history where term<? order by term desc limit 1";
	public static String LAST10_SQL = "select * from Ssq_drawing_history where term<? order by term desc limit 10";
	public static String LAST150_SQL = "select * from Ssq_drawing_history where term<? order by term desc limit 150";
	public static String LAST_DRAWING = "select * from Ssq_drawing_history order by term desc limit 1";
	
	public static String LAST_TERMS = "select * from Ssq_drawing_history where term>2016000 order by term desc";
	
	public static String REWARD_SQL = "select * from ssq_drawing_reward where term=?";
	
	public static Ssq_drawing_history getLastDrawing(){
		Ssq_drawing_history history = Ssq_drawing_history.me.findFirst(LAST_DRAWING);
		return history;
	}
	
	public static Ssq_drawing_reward getReward(int term){
		Ssq_drawing_reward reward = Ssq_drawing_reward.me.findFirst(REWARD_SQL, term);
		return reward;
	}
	
	public static int[] getLastTerms(int times){
		List<Ssq_drawing_history> last = Ssq_drawing_history.me.find(LAST_TERMS);
		int num = times<last.size()?times:last.size();
		int[] result = new int[num];
		for(int i=0;i<num;i++){
			result[i] = last.get(i).getItem(0);
		}
		return result;
	}
	
	public static Ssq_drawing_history get(int term){
		Ssq_drawing_history history = Ssq_drawing_history.me.findFirst(SQL, term);
		return history;
	}
	
	public static Ssq_drawing_history getLast(int term){
		Ssq_drawing_history last = null;
		
		last = Ssq_drawing_history.me.findFirst(LAST_SQL, term);
		
		return last;
	}
	
	public static List<Ssq_drawing_history> getLast10(int term){
		
		List<Ssq_drawing_history> last = Ssq_drawing_history.me.find(LAST10_SQL, term);
		
		return last;
	}
	
	public static List<Ssq_drawing_history> getLast150(int term){
		List<Ssq_drawing_history> last = Ssq_drawing_history.me.find(LAST150_SQL, term);
		return last;
	}
	
	public static List<Ssq_drawing_history> getDrawing(int term, int times){
		String sql = "select * from Ssq_drawing_history where term<? order by term desc limit "+times;
		List<Ssq_drawing_history> last = Ssq_drawing_history.me.find(sql, term);
		return last;
	}
}
