package com.houxm.caipiao.util;

import java.util.List;

import com.houxm.caipiao.model.Ssq_drawing_history;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

/**
 * 对所有的开奖号码进行先后排序
 * @author 学明
 *
 */
public class DrawingSortUtil {

	public static void main(String[] args) {
		PropKit.use("a_little_config.txt");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"),
		PropKit.get("user"),
		PropKit.get("password").trim());
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.addMapping("ssq_drawing_history", Ssq_drawing_history.class);
		arp.start();
		
		String sql = "SELECT * FROM ssq_drawing_history";
		List<Ssq_drawing_history> list = Ssq_drawing_history.me.find(sql);
		for(Ssq_drawing_history history:list){
			int[] red = sortRed(history);
			history.set("red1", red[0]);
			history.set("red2", red[1]);
			history.set("red3", red[2]);
			history.set("red4", red[3]);
			history.set("red5", red[4]);
			history.set("red6", red[5]);
			history.update();
		}
	}

	private static int[] sortRed(Ssq_drawing_history history) {
		int[] red = new int[6];
		red[0] = history.getItem(1);
		red[1] = history.getItem(2);
		red[2] = history.getItem(3);
		red[3] = history.getItem(4);
		red[4] = history.getItem(5);
		red[5] = history.getItem(6);
		
		//冒泡排序
		for(int i=0;i<5;i++){
			for(int j=i+1;j<6;j++){
				if(red[i]>red[j]){
					int temp = red[i];
					red[i] = red[j];
					red[j] = temp;
				}
			}
		}
		return red;
	}

}
