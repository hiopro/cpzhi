package com.houxm.caipiao.util;

import java.util.List;

/**
 * 将list的号球拼成以逗号隔开的字符串
 * @author 学明
 *
 */
public class BallUtil {

	public static String toString(List<Integer> result){
		String temp = "";
		for(int i=0;i<result.size();i++){
			if(i==result.size()-1){
				temp += result.get(i);
			}else{
				temp += result.get(i)+",";
			}
		}
		return temp;
	}
}
