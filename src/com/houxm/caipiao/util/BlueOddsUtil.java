package com.houxm.caipiao.util;

public class BlueOddsUtil {

private float[] odds = null;
	
	/**
	 * 初始化，默认每个号球100分基础分。
	 */
	public BlueOddsUtil(){
		odds = buildNew100Odds17();
	}
	/**
	 * 制造16个蓝球的出球几率的float数组，默认几率为100
	 * @return
	 */
	public static float[] buildNew100Odds17(){
		float[] f = new float[17];
		for(int i=1;i<17;i++){
			f[i] = 100.0f;
		}
		return f;
	}
	
	/**
	 * 制造16个蓝球的出球几率的float数组，默认几率为0
	 * @return
	 */
	public static float[] buildNewOdds17(){
		float[] f = new float[17];
		for(int i=1;i<17;i++){
			f[i] = 0.0f;
		}
		return f;
	}
	
	/**
	 * 制造16个蓝球的出球号球的int数组，默认为1~16号球
	 * @return
	 */
	public static int[] buildNewBall17(){
		int[] ball = new int[17];
		for(int i=1;i<17;i++){
			ball[i] = i;
		}
		return ball;
	}
	
	/**
	 * 以当前对象的基础分为基本，加上参数的积分。
	 * @param plus
	 */
	public void plus(float[] plus){
		for(int i=1;i<17;i++){
			odds[i] += plus[i];
		}
	}
	
	/**
	 * 作为工具方法，把plus1和plus2的积分相加后，返回plus1
	 * @param plus
	 */
	public static float[] plus(float[] plus1, float[] plus2){
		for(int i=1;i<17;i++){
			plus1[i] += plus2[i];
		}
		return plus1;
	}
	
	/**
	 * 以当前对象的基础分为基本，加上参数的times倍的积分。
	 * @param plus
	 * @param times
	 */
	public void plus(float[] plus, int times){
		for(int i=1;i<17;i++){
			odds[i] += plus[i] * times;
		}
	}
	
	/**
	 * 两个RedOddsUtil对象的积分相加，并减掉一个基础分100分。
	 * @param oddsUtil
	 */
	public void plus(RedOddsUtil oddsUtil){
		for(int i=1;i<17;i++){
			odds[i] += oddsUtil.getOddsByIndex(i) - 100.0f;
		}
	}
	
	/**
	 * 两个RedOddsUtil对象的积分相加，并将参数中的积分减掉一个基础分100分后乘以times倍，再相加。
	 * @param oddsUtil
	 * @param times
	 */
	public void plus(RedOddsUtil oddsUtil, int times){
		for(int i=1;i<17;i++){
			odds[i] += (oddsUtil.getOddsByIndex(i) - 100.0f) * times;
		}
	}
	
	/**
	 * 获取当前对象中的指定索引的积分值。
	 * @param index
	 * @return
	 */
	public float getOddsByIndex(int index){
		return odds[index];
	}
	
	/**
	 * 将当前对象中按出球积分的从大到小排序后的出球情况，以逗号隔开返回。
	 * @return
	 */
	public String ballToString(){
		String ball = "";
		int[] balls = buildNewBall17();
		float[] rates = buildNewOdds17();
		for(int i=1;i<17;i++){
			rates[i] = odds[i];
		}
		
		//冒泡排序
		for(int i=1;i<16;i++){
			for(int j=i+1;j<17;j++){
				if(rates[i]<rates[j]){
					float temp = rates[i];
					rates[i] = rates[j];
					rates[j] = temp;
					
					int tempBall = balls[i];
					balls[i] = balls[j];
					balls[j] = tempBall;
				}
			}
		}
		
		for(int i=1;i<17;i++){
			if(i==16){
				ball += balls[i];
			}else{
				ball += balls[i]+",";
			}
		}
		
		return ball;
	}
	
	/**
	 * 将当前对象中按出球积分的从大到小排序后的积分情况，以逗号隔开返回。
	 * @return
	 */
	public String rateToString(){
		String rate = "";
		float[] rates = buildNewOdds17();
		for(int i=1;i<17;i++){
			rates[i] = odds[i];
		}
		
		//冒泡排序
		for(int i=1;i<16;i++){
			for(int j=i+1;j<17;j++){
				if(rates[i]<rates[j]){
					float temp = rates[i];
					rates[i] = rates[j];
					rates[j] = temp;
				}
			}
		}
		
		for(int i=1;i<17;i++){
			if(i==16){
				rate += rates[i];
			}else{
				rate += rates[i]+",";
			}
		}
		
		return rate;
	}
}
