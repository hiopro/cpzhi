package com.houxm.caipiao.controller;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.model.Ssq_blue_method;
import com.houxm.caipiao.model.Ssq_blue_plan_analysis;
import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_drawing_reward;
import com.houxm.caipiao.model.Ssq_group_plan_forecast;
import com.houxm.caipiao.model.Ssq_red_method;
import com.houxm.caipiao.model.Ssq_red_plan_analysis;
import com.houxm.caipiao.service.LoadNextService;
import com.houxm.caipiao.service.SetData;
import com.houxm.caipiao.util.DrawingHistoryUtil;
import com.houxm.caipiao.util.ForecastUtil;
import com.houxm.caipiao.util.NextTermUtil;
import com.houxm.caipiao.vo.SsqPlanAnalysisVO;
import com.jfinal.core.Controller;

/**
 * 
 * @author 学明
 *
 */
public class IndexController extends Controller {
	
//	private static final Logger log = Logger.getLogger(IndexController.class);
	public void index() {
		int nextTerm = NextTermUtil.next();
		Ssq_drawing_history history = DrawingHistoryUtil.getLastDrawing();
		Ssq_drawing_reward reward = DrawingHistoryUtil.getReward(history.getInt("term"));
		int[] red = ForecastUtil.getForecastRed(nextTerm);
		float[] odds = ForecastUtil.getForecastRedOdds(nextTerm);
		int[] blue = ForecastUtil.getForecastBlue(nextTerm);
		List<Ssq_group_plan_forecast> group = ForecastUtil.getForecastGroup(nextTerm);
		setAttr("nextTerm", nextTerm);
		setAttr("history", history);
		setAttr("reward", reward);
		setAttr("red", red);
		setAttr("odds", odds);
		setAttr("blue", blue);
		setAttr("group", group);
		render("index.html");
	}
	
	public void history() {
		Ssq_drawing_history history = DrawingHistoryUtil.getLastDrawing();
		Ssq_drawing_reward reward = null;
		String termStr = this.getPara("term");
		int term = 0;
		if(termStr!=null && !"".equals(termStr)){
			term = Integer.parseInt(termStr);
		}
		if(term<2016000){
			term = history.getItem(0);
		}
		if(term!=history.getItem(0)){
			history = DrawingHistoryUtil.get(term);
			reward = DrawingHistoryUtil.getReward(term);
		}else{
			reward = DrawingHistoryUtil.getReward(history.getItem(0));
		}
		int[] red = ForecastUtil.getForecastRed(term);
		float[] odds = ForecastUtil.getForecastRedOdds(term);
		int[] blue = ForecastUtil.getForecastBlue(term);
		int[] terms = DrawingHistoryUtil.getLastTerms(10);
		
		String redSql = "select * from Ssq_red_plan_analysis where term=?";
		Ssq_red_plan_analysis ra = Ssq_red_plan_analysis.me.findFirst(redSql, term);
		String blueSql = "select * from Ssq_blue_plan_analysis where term=?";
		Ssq_blue_plan_analysis ba = Ssq_blue_plan_analysis.me.findFirst(blueSql, term);
		
		String rsql = "select * from Ssq_red_plan_analysis where term<=? order by term desc limit 100";
		List<Ssq_red_plan_analysis> ras = Ssq_red_plan_analysis.me.find(rsql, term);
		
		String bsql = "select * from Ssq_blue_plan_analysis where term<=? order by term desc limit 100";
		List<Ssq_blue_plan_analysis> bas = Ssq_blue_plan_analysis.me.find(bsql, term);
		
		List<SsqPlanAnalysisVO> list = new ArrayList<SsqPlanAnalysisVO>();
		if(ras!=null){
			for(int i=0;i<ras.size();i++){
				Ssq_red_plan_analysis r = ras.get(i);
				Ssq_blue_plan_analysis b = bas.get(i);
				list.add(SsqPlanAnalysisVO.build(r, b));
			}
		}
		setAttr("terms", terms);
		setAttr("term", term);
		setAttr("history", history);
		setAttr("reward", reward);
		setAttr("red", red);
		setAttr("odds", odds);
		setAttr("blue", blue);
		setAttr("ra", ra);
		setAttr("ba", ba);
		setAttr("spav", list);
		render("history.html");
	}
	
	public void updDrawing(){
		boolean flag = LoadNextService.loadNext();
		renderText(flag+"");
	}
	
	public void setOldData(){
		SetData.set();
		renderText("over");
	}
	
	public void forecast(){
		String redSql = "select * from ssq_red_method";
		List<Ssq_red_method> srm = Ssq_red_method.me.find(redSql);
		String blueSql = "select * from ssq_blue_method";
		List<Ssq_blue_method> sbm = Ssq_blue_method.me.find(blueSql);
		setAttr("srm", srm);
		setAttr("sbm", sbm);
		render("forecast.html");
	}
	
	public void about(){
		render("about.html");
	}
}
