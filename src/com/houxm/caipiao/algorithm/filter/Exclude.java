package com.houxm.caipiao.algorithm.filter;

import java.util.List;

import com.houxm.caipiao.model.Ssq_matrix_method_forecast;

public interface Exclude {

	public List<Ssq_matrix_method_forecast> exclude(List<Ssq_matrix_method_forecast> forecast, int term);
}
