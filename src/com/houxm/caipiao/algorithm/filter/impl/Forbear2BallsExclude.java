/**
 * 红球生克表，排除红球组合。
 * 在150期历史开奖号码的数据支撑下，可以找到小于1或等于0的号码组合，该组合出现概率很小，应排除该组合。
 */
package com.houxm.caipiao.algorithm.filter.impl;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.filter.Exclude;
import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_matrix_method_forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class Forbear2BallsExclude implements Exclude {
	
	//生克表数组
	private int[][] values = null;

	/**
	 * 获取参数（预测的开奖号码），根据生克表对预测号码进行过滤。
	 */
	@Override
	public List<Ssq_matrix_method_forecast> exclude(
			List<Ssq_matrix_method_forecast> forecast, int term) {
		init(term);
		List<Ssq_matrix_method_forecast> ls = new ArrayList<Ssq_matrix_method_forecast>();
		for(Ssq_matrix_method_forecast every:forecast)
		{
			//有组合15种，如出现一个组合的值小于2时，则认为该红球组合相克，应删除
			int total = 1;
			if(values[every.getBall(1)][every.getBall(2)]<total
					|| values[every.getBall(1)][every.getBall(3)]<total
					|| values[every.getBall(1)][every.getBall(4)]<total
					|| values[every.getBall(1)][every.getBall(5)]<total
					|| values[every.getBall(1)][every.getBall(6)]<total
					|| values[every.getBall(2)][every.getBall(3)]<total
					|| values[every.getBall(2)][every.getBall(4)]<total
					|| values[every.getBall(2)][every.getBall(5)]<total
					|| values[every.getBall(2)][every.getBall(6)]<total
					|| values[every.getBall(3)][every.getBall(4)]<total
					|| values[every.getBall(3)][every.getBall(5)]<total
					|| values[every.getBall(3)][every.getBall(6)]<total
					|| values[every.getBall(4)][every.getBall(5)]<total
					|| values[every.getBall(4)][every.getBall(6)]<total
					|| values[every.getBall(5)][every.getBall(6)]<total)
			{
				//符合生克表
				ls.add(every);
			}
		}
		forecast.removeAll(ls);
		return forecast;
	}
	
	/**
	 * 初始化生克表。
	 * @param dao 
	 */
	public void init(int term)
	{
		values = new int[34][34];
		//初始化数组
		for(int i=0;i<34;i++)
		{
			for(int j=0;j<34;j++)
			{
				if(j==0)
					values[i][j] = i;
				else
					values[i][j] = 0;
			}
		}
		//获取最新150期开奖数据
		List<Ssq_drawing_history> last150 = DrawingHistoryUtil.getLast150(term);
		for(Ssq_drawing_history every:last150)
		{
			//从6个数任选2个组合共有多少组
			//共有15组，如：AB,AC,AD,AE,AF,BC,BD,BE,BF,CD,CE,CF,DE,DF,EF.
			values[every.getItem(1)][every.getItem(2)]++;
			values[every.getItem(1)][every.getItem(3)]++;
			values[every.getItem(1)][every.getItem(4)]++;
			values[every.getItem(1)][every.getItem(5)]++;
			values[every.getItem(1)][every.getItem(6)]++;
			values[every.getItem(2)][every.getItem(3)]++;
			values[every.getItem(2)][every.getItem(4)]++;
			values[every.getItem(2)][every.getItem(5)]++;
			values[every.getItem(2)][every.getItem(6)]++;
			values[every.getItem(3)][every.getItem(4)]++;
			values[every.getItem(3)][every.getItem(5)]++;
			values[every.getItem(3)][every.getItem(6)]++;
			values[every.getItem(4)][every.getItem(5)]++;
			values[every.getItem(4)][every.getItem(6)]++;
			values[every.getItem(5)][every.getItem(6)]++;
		}
	}

}
