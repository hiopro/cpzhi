/**
 * 差和值
 * 指的是每期红球的和值-102的值。
 * 5期差和值大于35，10期差和值大于75时，认为会出现纠偏。
 */
package com.houxm.caipiao.algorithm.filter.impl;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.filter.Exclude;
import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_matrix_method_forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class SumExclude implements Exclude {

	public List<Integer> sum = new ArrayList<Integer>();
	public int sum5 = 0;
	public int sum10 = 0;
	
	@Override
	public List<Ssq_matrix_method_forecast> exclude(
			List<Ssq_matrix_method_forecast> forecast, int term) {
		initSum(term);
		int sum102 = 0;
		if(sum5>=35 && sum10>=75)
			sum102 = 1;
		if(sum5<=-35 && sum10<=-75)
			sum102 = 2;
		List<Ssq_matrix_method_forecast> ls = new ArrayList<Ssq_matrix_method_forecast>();
		for(Ssq_matrix_method_forecast l:forecast)
		{
			int num = l.getBall(1)+l.getBall(2)+l.getBall(3)+l.getBall(4)+l.getBall(5)+l.getBall(6);
			if(sum102==0)
				break;
			else if(sum102==1)
			{
				//预测为和值小于102
				if(num>102)
					ls.add(l);
			}else{
				//预测为和值小于102
				if(num<102)
					ls.add(l);
			}
		}
		forecast.removeAll(ls);
		return forecast;
	}
	
	public void initSum(int term)
	{
		List<Ssq_drawing_history> lotteries = DrawingHistoryUtil.getDrawing(term, 20);
		for(int i=0;i<20;i++)
		{
			Ssq_drawing_history l = lotteries.get(i);
			int num = l.getItem(1)+l.getItem(2)+l.getItem(3)+l.getItem(4)+l.getItem(5)+l.getItem(6);
			sum.add(num-102);
		}
		
		for(int i=0;i<10;i++)
		{
			if(i<5)
				sum5 += sum.get(i);
			
			sum10 += sum.get(i);
		}
	}

}
