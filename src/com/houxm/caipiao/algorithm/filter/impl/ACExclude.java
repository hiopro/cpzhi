/**
 * 根据双色球的AC概率分布判断筛选组合；根据双色球的AC趋势判断筛选组合。
 * 如开奖号码是4、21、23、31、32、33，计算这6个红球的AC值。
 * 将6个号码进行组合相减（获得正整数），比如21-4=17；23-4=19等等，最后获得0、1、2、8、9、10、12、17、19、20、
 * 21、27、28、29等14个数值，14减去正选号码6（6个红球）再减去1得到数值7，7即为AC值。
 * 根据经验，AC值一般为7、8，较少出现6、9，其他值则非常少。
 */
package com.houxm.caipiao.algorithm.filter.impl;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.filter.Exclude;
import com.houxm.caipiao.model.Ssq_matrix_method_forecast;

public class ACExclude implements Exclude {

	/**
	 * 获取参数（预测的开奖号码），根据AC概率分布（过滤小于10000注以下的号码）对预测号码进行过滤。
	 * @param lotteries
	 * @param dao 
	 * @return
	 */
	public List<Ssq_matrix_method_forecast> exclude(List<Ssq_matrix_method_forecast> forecast, int term)
	{
		List<Ssq_matrix_method_forecast> ls = new ArrayList<Ssq_matrix_method_forecast>();
		for(Ssq_matrix_method_forecast every:forecast)
		{
			int ac = getLotteryAC(every);
			/**
			 * AC值的概率分布
			 * A C0=93             A C6=184858注
			 * A C1=437注                     A C7=224408注
			 * A C2=3093注                   A C8=339540注
			 * A C3=9642注                   A C9=158760注
			 * A C4=32735注                 A C10=85308注
			 * A C5=68694注 
			 */
			if(ac<3)
			{
				ls.add(every);
			}
		}
		forecast.removeAll(ls);
		
		return forecast;
	}

	/**
	 * 获取该期次中奖号码的AC值,
	 * 所有不重复差值的个数-(6-1)=AC值
	 * @param every
	 * @return
	 */
	private int getLotteryAC(Ssq_matrix_method_forecast every) {
		//共有15组，如：AB,AC,AD,AE,AF,BC,BD,BE,BF,CD,CE,CF,DE,DF,EF.
		List<Integer> list = new ArrayList<Integer>();
		int ab = every.getBall(2)-every.getBall(1);
		int ac = every.getBall(3)-every.getBall(1);
		int ad = every.getBall(4)-every.getBall(1);
		int ae = every.getBall(5)-every.getBall(1);
		int af = every.getBall(6)-every.getBall(1);
		int bc = every.getBall(3)-every.getBall(2);
		int bd = every.getBall(4)-every.getBall(2);
		int be = every.getBall(5)-every.getBall(2);
		int bf = every.getBall(6)-every.getBall(2);
		int cd = every.getBall(4)-every.getBall(3);
		int ce = every.getBall(5)-every.getBall(3);
		int cf = every.getBall(6)-every.getBall(3);
		int de = every.getBall(5)-every.getBall(4);
		int df = every.getBall(6)-every.getBall(4);
		int ef = every.getBall(6)-every.getBall(5);
		
		list.add(ab);
		if(!list.contains(ac))
			list.add(ac);
		if(!list.contains(ad))
			list.add(ad);
		if(!list.contains(ae))
			list.add(ae);
		if(!list.contains(af))
			list.add(af);
		if(!list.contains(bc))
			list.add(bc);
		if(!list.contains(bd))
			list.add(bd);
		if(!list.contains(be))
			list.add(be);
		if(!list.contains(bf))
			list.add(bf);
		if(!list.contains(cd))
			list.add(cd);
		if(!list.contains(ce))
			list.add(ce);
		if(!list.contains(cf))
			list.add(cf);
		if(!list.contains(de))
			list.add(de);
		if(!list.contains(df))
			list.add(df);
		if(!list.contains(ef))
			list.add(ef);
		return list.size()-(6-1);
	}
	
}
