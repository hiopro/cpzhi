/**
 * 一般开奖号码的尾数个数为4、5、6个，尾数个数为2、3个，一般可以排除。
 */
package com.houxm.caipiao.algorithm.filter.impl;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.filter.Exclude;
import com.houxm.caipiao.model.Ssq_matrix_method_forecast;

public class EndNumberExclude implements Exclude {
	
	@Override
	public List<Ssq_matrix_method_forecast> exclude(
			List<Ssq_matrix_method_forecast> forecast, int term) {
		//尾数个数是2、3个的先过滤掉。
		List<Ssq_matrix_method_forecast> ls = new ArrayList<Ssq_matrix_method_forecast>();
		for(Ssq_matrix_method_forecast l:forecast)
		{
			if(this.getEndNumber(l).size()<4)
			{
				ls.add(l);
			}
		}
		forecast.removeAll(ls);
		return forecast;
	}
	
	/**
	 * 获取该期次的尾数。
	 * @param l
	 * @return
	 */
	public List<Integer> getEndNumber(Ssq_matrix_method_forecast l)
	{
		List<Integer> end = new ArrayList<Integer>();
		int temp = l.getBall(1)%10;
		end.add(temp);
		
		temp = l.getBall(2)%10;
		if(!end.contains(temp))
			end.add(temp);
		
		temp = l.getBall(3)%10;
		if(!end.contains(temp))
			end.add(temp);
		
		temp = l.getBall(4)%10;
		if(!end.contains(temp))
			end.add(temp);
		
		temp = l.getBall(5)%10;
		if(!end.contains(temp))
			end.add(temp);
		
		temp = l.getBall(6)%10;
		if(!end.contains(temp))
			end.add(temp);
		
		return end;
	}

}
