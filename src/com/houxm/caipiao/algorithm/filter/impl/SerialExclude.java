/**
 * 连号组合排除法。
 * 连号出现一般比无连号大3倍。
 * 4、5连号概率很小，6连号可以忽略。
 * 没有区分3连号、4连号、甚至5、6连号。
 */
package com.houxm.caipiao.algorithm.filter.impl;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.filter.Exclude;
import com.houxm.caipiao.model.Ssq_matrix_method_forecast;

public class SerialExclude implements Exclude {

	@Override
	public List<Ssq_matrix_method_forecast> exclude(
			List<Ssq_matrix_method_forecast> forecast, int term) {
		List<Ssq_matrix_method_forecast> ls = new ArrayList<Ssq_matrix_method_forecast>();
		for(Ssq_matrix_method_forecast l:forecast)
		{
			int serial = getSerial(l);
			if(serial>3)
			{
				ls.add(l);
			}
		}
		
		forecast.removeAll(ls);
		return forecast;
	}
	
	private int getSerial(Ssq_matrix_method_forecast l) {
		int serial = 0;
		if(l.getBall(1)+1==l.getBall(2))
		{
			serial++;
		}
		if(l.getBall(2)+1==l.getBall(3))
		{
			serial++;
		}
		if(l.getBall(3)+1==l.getBall(3))
		{
			serial++;
		}
		if(l.getBall(4)+1==l.getBall(5))
		{
			serial++;
		}
		if(l.getBall(5)+1==l.getBall(6))
		{
			serial++;
		}
		return serial;
	}

}
