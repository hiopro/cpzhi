package com.houxm.caipiao.algorithm.filter.impl;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.filter.Exclude;
import com.houxm.caipiao.model.Ssq_matrix_method_forecast;

/**
 * 去掉极端情况，所选的6个号球全部为mode3的值一个区间段的值。
 * @author 学明
 *
 */
public class ExtremeExclude implements Exclude {

	@Override
	public List<Ssq_matrix_method_forecast> exclude(
			List<Ssq_matrix_method_forecast> forecast, int term) {
		List<Ssq_matrix_method_forecast> ls = new ArrayList<Ssq_matrix_method_forecast>();
		for(Ssq_matrix_method_forecast l:forecast)
		{
			int[] temp = new int[]{0,0,0};
			for(int k=1;k<7;k++)
			{
				int red = l.getBall(k);
				temp[red%3]++;
			}
			if(temp[0]==6 || temp[1]==6 || temp[2]==6)
				ls.add(l);
		}
		forecast.removeAll(ls);
		return forecast;
	}
	
}
