package com.houxm.caipiao.algorithm.red.impl;
/**
 * 减尾数拆分和值法：以最后一期次的双色球中奖号码，将所有红球的尾数相加，得出的值如果大于10再个位十位相加后，
 * 得到的值为减尾数拆分和值。以此值与最后一期次的中奖号码相减得到的结果，有5/6的几率在下一期不会出现。
 * kill少量bad号球
 */
import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.model.Ssq_drawing_history;

public class TailSumForecast implements Forecast {
	
	/**
	 * 默认1期
	 */
	public TailSumForecast()
	{
	}
	
	/**
	 * 根据双色球开奖号码，获得尾和值。
	 * @param lottery
	 * @return
	 */
	private int getTailSum(Ssq_drawing_history lottery) {
		int tailSum = 0;
		for(int i=1;i<7;i++){
			tailSum += lottery.getItem(1)%10;
		}
		if(tailSum>9)
			tailSum = tailSum/10 + tailSum%10;
		return tailSum;
	}

	@Override
	public List<Integer> forecast(int term) {
		List<Integer> result = new ArrayList<Integer>();
		//最新1期开奖号码，倒序的1期次开出的红球号码
		String sql = "select * from ssq_drawing_history where term<"+term+" order by term desc limit 1";
		Ssq_drawing_history last = Ssq_drawing_history.me.findFirst(sql);
		int tailSum = this.getTailSum(last);
		for(int i=1;i<7;i++){
			int red = last.getItem(i);
			int temp = 0;
			if(red>tailSum){
				temp = red - tailSum;
			}else if(red<tailSum){
				temp = tailSum - red;
			}
			result.add(temp);
		}
		return result;
	}
}
