package com.houxm.caipiao.algorithm.red.impl.killer;

/**
 * 红球杀手,蓝球为偶数时，结果为蓝球*2+2；蓝球为奇数时，结果为蓝球*5+2.该结果下期可能不出现。
 */
import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.model.Ssq_drawing_history;

public class RedKillerbluemode2Forecast implements Forecast {
	public static int TERM_TIMES = 1;

	/**
	 * 默认1期
	 */
	public RedKillerbluemode2Forecast() {
	}

	/**
	 * 指定某termTimes期
	 */
	public RedKillerbluemode2Forecast(int termTimes) {
		TERM_TIMES = termTimes;
	}

	/**
	 * 获取预测的bad红球号码。
	 * 
	 * @param currentTerm
	 *            当前预测期次
	 * @return
	 */
	public List<Integer> forecast(int term) {
		List<Integer> result = new ArrayList<Integer>();
		// 最新1期开奖号码，倒序的1期次开出的红球号码
		String sql = "select * from ssq_drawing_history where term<" + term
				+ " order by term desc limit 1";
		Ssq_drawing_history last = Ssq_drawing_history.me.findFirst(sql);
		int blue = last.getItem(7);
		int red = 0;
		if (blue % 2 == 0) {
			red = blue * 2 + 2;
		} else {
			red = blue * 5 + 2;
		}
		if (red < 0)
			red += 33;
		if (red > 33)
			red = red % 33;
		if (red == 0)
			red = 33;
		result.add(red);
		return result;
	}
}
