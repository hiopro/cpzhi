package com.houxm.caipiao.algorithm.red.impl.killer;

/**
 * 红球杀手,第3个红球-第1个红球+蓝球+2的结果，下期可能不出现。
 */
import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.model.Ssq_drawing_history;

public class RedKiller31blue2Forecast implements Forecast {
	public static int TERM_TIMES = 1;

	/**
	 * 默认1期
	 */
	public RedKiller31blue2Forecast() {
	}

	/**
	 * 指定某termTimes期
	 */
	public RedKiller31blue2Forecast(int termTimes) {
		TERM_TIMES = termTimes;
	}

	/**
	 * 获取预测的bad红球号码。
	 * 
	 * @param currentTerm
	 *            当前预测期次
	 * @return
	 */
	public List<Integer> forecast(int term) {
		List<Integer> result = new ArrayList<Integer>();
		// 最新1期开奖号码，倒序的1期次开出的红球号码
		String sql = "select * from ssq_drawing_history where term<" + term
				+ " order by term desc limit 1";
		Ssq_drawing_history last = Ssq_drawing_history.me.findFirst(sql);
		int red = last.getItem(3) - last.getItem(1) + last.getItem(7) + 2;
		if (red < 0)
			red += 33;
		if (red > 33)
			red = red % 33;
		if (red == 0)
			red = 33;
		result.add(red);
		return result;
	}
}
