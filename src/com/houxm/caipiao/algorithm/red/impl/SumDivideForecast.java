package com.houxm.caipiao.algorithm.red.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.util.DrawingHistoryUtil;

/**
 * 第一步，将上期双色球红球六个开奖号码列出来，例如要挑选2007133期奖号，将上期，即第2007132期开奖号码01、09、16、21、22、23+05列出来; 
 * 第二步，将六个红球加起来算出其和值，为1+9+16+21+22+23=92; 
 * 第三步，用和值依次减去每个红球，除以每个红球，所得的商的个位数，然后取同尾数，作为下期选择的奖号范围。 
 * (92-1)÷1=91;91的个位是1的同尾数是：1、11、21、31; 
 * (92-9)÷9=9……余2;9的个位是9的同尾数是：9、19、29;
 * (92-16)÷16=4……余7;4的个位是4的同尾数是：4、14、24;
 * (92-21)÷21=3……余3;3的个位是3的同尾数是：3、13、23、33;
 * (92-22)÷22=3……余1，3的个位是3的同尾数是：3、13、23、33;
 * (92-23)÷23=3……余0;3的个位是3的同尾数是：3、13、23、33;
 * 综上所述，以下号码：1、11、21、31、9、19、29、4、14、24、3、13、23、33，是双色球第2007133期备选号码。其中有三组均出现3，必然有3尾数出现。
 * 双色球第2007133期开奖号码：03、06、07、11、13、33+10，四个号码包含在其中，尾数3开出3枚奖号。
 * 这种选号方法不是期期能够命中四到五个，但至少命中两个以上号码。虽然有1-3个不按这种特征出现，如果凭借长期的经验积累和凭技巧观察这几个未知数时，准确度大一些，注意机遇可遇不可求，不能生搬硬套。

 * @author 学明
 *
 */
public class SumDivideForecast implements Forecast {

	@Override
	public List<Integer> forecast(int term) {
		Ssq_drawing_history history = DrawingHistoryUtil.getLast(term);
		int sum = 0;
		for(int i=1;i<7;i++){
			sum += history.getItem(i);
		}
		
		List<Integer> list = new ArrayList<Integer>();
		Set<Integer> set = new HashSet<Integer>();
		for(int i=1;i<7;i++){
			int tmp = quotient(sum, history.getItem(i));
			set.add(tmp);
		}
		
		Iterator<Integer> iter = set.iterator();
		while(iter.hasNext()){
			int tmp = iter.next();
			List<Integer> list2 = tailNum(tmp);
			list.addAll(list2);
		}
		
		return list;
	}

	private List<Integer> tailNum(Integer next) {
		List<Integer> num = new ArrayList<Integer>();
		switch(next)
		{
			case 0:
				num.add(10);
				num.add(20);
				num.add(30);
				break;
			case 1:
				num.add(1);
				num.add(11);
				num.add(21);
				num.add(31);
				break;
			case 2:
				num.add(2);
				num.add(12);
				num.add(22);
				num.add(32);
				break;
			case 3:
				num.add(3);
				num.add(13);
				num.add(23);
				num.add(33);
				break;
			case 4:
				num.add(4);
				num.add(14);
				num.add(24);
				break;
			case 5:
				num.add(5);
				num.add(15);
				num.add(25);
				break;
			case 6:
				num.add(6);
				num.add(16);
				num.add(26);
				break;
			case 7:
				num.add(7);
				num.add(17);
				num.add(27);
				break;
			case 8:
				num.add(8);
				num.add(18);
				num.add(28);
				break;
			case 9:
				num.add(9);
				num.add(19);
				num.add(29);
				break;
		}
		
		return num;
	}

	/**
	 * 求商
	 * @param sum
	 * @param red
	 * @return
	 */
	private int quotient(int sum, int red) {
		int tmp = (sum-red)/red;
		tmp = tmp%10;
		return tmp;
	}

}
