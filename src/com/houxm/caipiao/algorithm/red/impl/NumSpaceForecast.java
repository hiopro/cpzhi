package com.houxm.caipiao.algorithm.red.impl;

/**
 * 号码间隔选号法。
 * 号码间隔是：如本期开了02，上期也开了02，则认为02的间隔为0；再如本期开了05，而上期没开，在上上期开了05，则认为间隔为1。如果大于5期，则认为间隔是5。
 * 而对于本期预测号码，间隔是0的就是上期开奖号码；间隔为1的，就是上上期的开奖号码（要去掉上期开奖号码）；以此类推。
 * 而我们要统计5期开奖号码的间隔数量。即统计间隔为0的有几个， 间隔为1的有几个，一直到间隔为4的有几个（间隔为5不统计）
 * 在统计完成后，我们认为间隔的数量最少的和最多的，在预测期次比较容易出现。
 */
import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.model.Ssq_drawing_history;

public class NumSpaceForecast implements Forecast {

	public List<Space> spaces = new ArrayList<Space>();
	public static int TERM_TIMES = 5;
	
	/**
	 * 默认1期
	 */
	public NumSpaceForecast()
	{
	}
	
	/**
	 * 指定某termTimes期
	 */
	public NumSpaceForecast(int termTimes)
	{
		TERM_TIMES = termTimes;
	}
	
	private void initNumSpace(List<Ssq_drawing_history> lasts) {
		//记录5期次，6个红球分别的间隔数
		int[][] numSpace = new int[5][6];
		for(int i=0;i<5;i++)
		{
			Ssq_drawing_history lottery = lasts.get(i);
			//循环每个红球，在它的上一期或更远的期次中查找红球，计算它的间隔数，如果间隔大于等于5，则认为间隔为5
			int term = lottery.getItem(0);
			for(int j=1;j<7;j++){
				int spa = 0;
				//是否已找到对应的红球间隔
				boolean flag = false;
				int red = lottery.getItem(j);
				for(int k=i+1;k<lasts.size();k++){
					Ssq_drawing_history tempL = lasts.get(k);
					for(int m=1;m<7;m++){
						int tempRed = tempL.getItem(m);
						if(red==tempRed)
						{
							spa = tempL.getItem(0)-term-1;
							flag = true;
						}
					}
					if( !flag && (tempL.getItem(0)-term-1) >= 5)
					{
						spa = 5;
						//间隔已超过或等于5，提醒程序跳出循环
						flag = true;
					}
					//找到间隔，跳出循环
					if(flag)
						break;
				}
				numSpace[i][j-1] = spa;
			}
		}
		
		//每个红球的间隔矩阵已生成
		//开始统计每个间隔对应的总个数，不统计间隔为5的，5的用不到
		int[] tempTotal = new int[5];
		for(int i=0;i<5;i++)
		{
			for(int j=0;j<5;j++){
				for(int k=0;k<6;k++){
					if(i==numSpace[j][k])
					{
						tempTotal[i] ++;
					}
				}
			}
		}
		
		//统计间隔为i的号球和个数，注意：此处统计本期预测期次与历史期次之间的间隔号球
		List<Integer> totalRed = new ArrayList<Integer>();
		Space space0 = new Space();
		space0.setSpace(0);
		List<Integer> num0 = new ArrayList<Integer>();
		for(int i=1;i<7;i++)
		{
			Ssq_drawing_history lottery = lasts.get(0);
			num0.add(lottery.getItem(i));
			totalRed.add(lottery.getItem(i));
		}
		space0.setNum(num0);
		space0.setTotal(tempTotal[0]);
		
		Space space1 = new Space();
		space1.setSpace(1);
		List<Integer> num1 = new ArrayList<Integer>();
		for(int i=1;i<7;i++)
		{
			Ssq_drawing_history lottery = lasts.get(1);
			int red = lottery.getItem(i);
			if(!totalRed.contains(red))
			{
				num1.add(red);
				totalRed.add(red);
			}
		}
		space1.setNum(num1);
		space1.setTotal(tempTotal[1]);
		
		Space space2 = new Space();
		space2.setSpace(2);
		List<Integer> num2 = new ArrayList<Integer>();
		for(int i=1;i<7;i++)
		{
			Ssq_drawing_history lottery = lasts.get(2);
			int red = lottery.getItem(i);
			if(!totalRed.contains(red))
			{
				num2.add(red);
				totalRed.add(red);
			}
		}
		space2.setNum(num2);
		space2.setTotal(tempTotal[2]);
		
		Space space3 = new Space();
		space3.setSpace(3);
		List<Integer> num3 = new ArrayList<Integer>();
		for(int i=1;i<7;i++)
		{
			Ssq_drawing_history lottery = lasts.get(3);
			int red = lottery.getItem(i);
			if(!totalRed.contains(red))
			{
				num3.add(red);
				totalRed.add(red);
			}
		}
		space3.setNum(num3);
		space3.setTotal(tempTotal[3]);
		
		Space space4 = new Space();
		space4.setSpace(4);
		List<Integer> num4 = new ArrayList<Integer>();
		for(int i=1;i<7;i++)
		{
			Ssq_drawing_history lottery = lasts.get(4);
			int red = lottery.getItem(i);
			if(!totalRed.contains(red))
			{
				num4.add(red);
				totalRed.add(red);
			}
		}
		space4.setNum(num4);
		space4.setTotal(tempTotal[4]);
		
		spaces.add(space0);
		spaces.add(space1);
		spaces.add(space2);
		spaces.add(space3);
		spaces.add(space4);
	}

	@Override
	public List<Integer> forecast(int term) {
		List<Integer> result = new ArrayList<Integer>();
		//最新TERM_TIMES期开奖号码，倒序的TERM_TIMES期次开出的红球号码
		String sql = "select * from ssq_drawing_history where term<"+term+" order by term desc limit "+(TERM_TIMES+6);
		List<Ssq_drawing_history> lasts = Ssq_drawing_history.me.find(sql);
		initNumSpace(lasts);
		//找到间隔的总个数的最大和最小值
		int tempMin = 0,tempMax = 0;
		for(int i=0;i<spaces.size();i++)
		{
			int temp = spaces.get(i).total;
			if(i==0)
			{
				tempMin = temp;
				tempMax = temp;
			}else{
				if(temp<tempMin)
				{
					tempMin = temp;
				}
				if(temp>tempMax)
				{
					tempMax = temp;
				}
			}
		}
		//根据间隔的总个数的最大和最小值，来获取对应的号球
		for(int i=0;i<spaces.size();i++)
		{
			Space space = spaces.get(i);
			if(space.getTotal()==tempMin)
			{
				result.addAll(space.getNum());
			}
			if(space.getTotal()==tempMax)
			{
				result.addAll(space.getNum());
			}
		}
		return result;
	}
}

class Space{
	int space;//间隔
	List<Integer> num;//号码
	int total;//该间隔5期内出现的总次数
	
	public int getSpace() {
		return space;
	}
	public void setSpace(int space) {
		this.space = space;
	}
	public List<Integer> getNum() {
		return num;
	}
	public void setNum(List<Integer> num) {
		this.num = num;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
}