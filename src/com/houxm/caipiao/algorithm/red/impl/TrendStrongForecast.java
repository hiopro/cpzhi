package com.houxm.caipiao.algorithm.red.impl;

/**
 * 号码趋势选号法。
 * 计算每一个红球的出现期次间隔。根据间隔的大小，找出该球的走势判断其评分。
 * 渐强型、强势型、反转型、趋弱型、弱势型、反复型
 * 本类中返回结果为 渐强型、强势型、反转型
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.model.Ssq_drawing_history;

public class TrendStrongForecast implements Forecast {

	//红球趋势
	public static Map<Integer, List<int[]>> trend = new HashMap<Integer, List<int[]>>();
	public int[] termLastSpace = new int[34];
	public static int TERM_TIMES = 100;
	
	/**
	 * 默认1期
	 */
	public TrendStrongForecast()
	{
	}
	
	/**
	 * 指定某termTimes期
	 */
	public TrendStrongForecast(int termTimes)
	{
		TERM_TIMES = termTimes;
	}
	
	private void initTrend(List<Ssq_drawing_history> lasts) {
		int termStart = lasts.get(lasts.size()-1).getItem(0);
		int termEnd = lasts.get(0).getItem(0);
		//用来记录当前处理的期次
		int[] termCurrent = new int[34];
		for(int i=1;i<34;i++)
		{
			termCurrent[i] = termStart;
		}
		
		//循环计算每个红球的间隔
		for(int i=1;i<34;i++)
		{
			List<int[]> red = new ArrayList<int[]>();
			for(Ssq_drawing_history lottery: lasts)
			{
				if(lottery.getItem(1)==i || lottery.getItem(2)==i || lottery.getItem(3)==i
						|| lottery.getItem(4)==i || lottery.getItem(5)==i || lottery.getItem(6)==i)
				{
					int[] temp = new int[3];
					temp[0] = lottery.getItem(0);
					temp[1] = lottery.getItem(0)-termCurrent[i];
					//1强（1~2）、2稍强（3~4）、3中（5~7）、4稍弱（8~9）、5弱（>9）
					temp[2] = this.getTrendNum(temp[1]);
					termCurrent[i] = lottery.getItem(0);
					red.add(temp);
				}
			}
			trend.put(i, red);
			//计算预测期次与上一间隔期次之间的间隔差
			termLastSpace[i] = (termEnd+1) - termCurrent[i];
		}
	}
	
	/**
	 * 获得该间隔的号码活跃度。
	 * @param i
	 * @return
	 */
	private int getTrendNum(int i) {
		//1强（1~2）、2稍强（3~4）、3中（5~7）、4稍弱（8~9）、5弱（>9）
		switch(i)
		{
			case 1: return 1;
			case 2: return 1;
			case 3: return 2;
			case 4: return 2;
			case 5: return 3;
			case 6: return 3;
			case 7: return 3;
			case 8: return 4;
			case 9: return 4;
			default: return 5;
		}
	}

	@Override
	public List<Integer> forecast(int term) {
		List<Integer> result = new ArrayList<Integer>();
		//最新TERM_TIMES期开奖号码，倒序的TERM_TIMES期次开出的红球号码
		String sql = "select * from ssq_drawing_history where term<"+term+" order by term desc limit "+TERM_TIMES;
		List<Ssq_drawing_history> lasts = Ssq_drawing_history.me.find(sql);
		this.initTrend(lasts);
		for(int i=1;i<34;i++)
		{
			List<int[]> red = trend.get(i);
			int lastTrend = this.getTrendNum(termLastSpace[i]);
			//1强（1~2）、2稍强（3~4）、3中（5~7）、4稍弱（8~9）、5弱（>9）
			int bits = red.get(red.size()-1)[2];
			int ten = red.get(red.size()-2)[2];
//			int hundred = red.get(red.size()-3)[2];
			if(bits<=2)
			{
				if(ten<=2)
				{
					if(lastTrend==3 || lastTrend==4){
						result.add(i);
					}
				}else if(ten==3 || ten==4){
					if(lastTrend<=3 ){
						result.add(i);
					}
				}else if(ten==5){
					if(lastTrend<=3 ){
						result.add(i);
					}
				}
			}else if(bits==3)
			{
				if(ten<=2)
				{
					if(lastTrend==3 || lastTrend==4){
						result.add(i);
					}
				}else if(ten==3 || ten==4){
					if(lastTrend>=5){
						result.add(i);
					}
				}
			}
		}
		return result;
	}

}
