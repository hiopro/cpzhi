package com.houxm.caipiao.algorithm.red.impl;

/**
 * 连续N期(默认6期)没出现过的尾号（mode 10 的结果值），下期有可能出。
 * 推荐少量bad号球
 */
import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.model.Ssq_drawing_history;

public class TailNumberBadForecast implements Forecast {

	public static int TERM_TIMES = 4;
	
	/**
	 * 默认最新的连续6期
	 */
	public TailNumberBadForecast()
	{
	}
	
	/**
	 * 最新的连续termTimes期
	 */
	public TailNumberBadForecast(int termTimes)
	{
		TERM_TIMES = termTimes;
	}
	
	public float forecasChance(int currentTerm) {
		float chance = 0.0f;
		return chance;
	}
	
	/**
	 * 获取该期次的各尾数的出现次数。
	 * 返回结果数组长度为10，如end[0]==1，则认为尾号为0的号球，出现1次。
	 * @param lottery
	 * @return
	 */
	public int[] getTailNumber(Ssq_drawing_history lottery)
	{
		int[] end = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		for(int i=1;i<7;i++){
			int redBall = lottery.getItem(i);
			int tail = redBall%10;
			end[tail] ++;
		}
		return end;
	}

	@Override
	public List<Integer> forecast(int term) {
		List<Integer> result = new ArrayList<Integer>();
		//最新TERM_TIMES期开奖号码，倒序的TERM_TIMES期次开出的红球号码
		String sql = "select * from ssq_drawing_history where term<"+term+" order by term desc limit "+TERM_TIMES;
		List<Ssq_drawing_history> lasts = Ssq_drawing_history.me.find(sql);
		//最新TERM_TIMES期开奖号码的红球的mode 10后的尾号出现个数的数组
		int[] tails = new int[10];
		for(int i=0;i<TERM_TIMES;i++)
		{
			int[] tail = this.getTailNumber(lasts.get(i));
			for(int j=0;j<10;j++)
			{
				tails[j] += tail[j];
			}
		}
		
		for(int i=1;i<34;i++)
		{
			int tail = i%10;
			//当前好球mode10后，其尾号对应的最近TERM_TIMES期出现次数为TERM_TIMES时，
			//则说明该号球在这一期不可能出现，加入结果中。
			if(tails[tail]==TERM_TIMES){
				result.add(i);
			}
		}
		return result;
	}

}
