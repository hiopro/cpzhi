package com.houxm.caipiao.algorithm.red.impl;

/**
 * 高频率选号法。
 * 选最近30期双色球，计算每个红球出现的次数，=5的为中频率的红球，>5的为高频率的红球，<5的为低频率的红球。
 * 根据经验，一、低频率的红球出现概率高于中频率和高频率的红球。二、如果某频率这期没出现，下期将强势出现。
 * 本类当上一期有频率的号球个数是0时，提供该频率的号球
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.model.Ssq_drawing_history;

public class HighFrequencyLast0Forecast implements Forecast {

	public static int TERM_TIMES = 30;
	
	/**
	 * 默认1期
	 */
	public HighFrequencyLast0Forecast()
	{
	}
	
	/**
	 * 指定某termTimes期
	 */
	public HighFrequencyLast0Forecast(int termTimes)
	{
		TERM_TIMES = termTimes;
	}
	
	/**
	 * 选最近30期双色球，计算每个红球出现的次数，=5的为中频率的红球，>5的为高频率的红球，<5的为低频率的红球。
	 * 返回high、medium、low为key的高、中、低三频率map对象。
	 * @return
	 */
	public Map<String, List<Integer>> getFrequency(List<Ssq_drawing_history> lasts)
	{
		int[] redSum = new int[34];
		List<Integer> high = new ArrayList<Integer>();
		List<Integer> medium = new ArrayList<Integer>();
		List<Integer> low = new ArrayList<Integer>();
		for(Ssq_drawing_history lottery: lasts)
		{
			for(int i=1;i<7;i++)
			{
				int red = lottery.getItem(i);
				redSum[red]++;
			}
		}
		for(int i=0;i<redSum.length;i++)
		{
			if(redSum[i]>5)
			{
				high.add(i);
			}else if(redSum[i]==5)
			{
				medium.add(i);
			}else{
				low.add(i);
			}
		}
		Map<String, List<Integer>> map = new HashMap<String, List<Integer>>();
		map.put("high", high);
		map.put("medium", medium);
		map.put("low", low);
		return map;
	}

	@Override
	public List<Integer> forecast(int term) {
		List<Integer> result = new ArrayList<Integer>();
		//最新TERM_TIMES期开奖号码，倒序的TERM_TIMES期次开出的红球号码
		String sql = "select * from ssq_drawing_history where term<"+term+" order by term desc limit "+TERM_TIMES;
		List<Ssq_drawing_history> lasts = Ssq_drawing_history.me.find(sql);
		Map<String, List<Integer>> map = this.getFrequency(lasts);
		List<Integer> high = map.get("high");
		List<Integer> medium = map.get("medium");
		List<Integer> low = map.get("low");
		
		
		
		//计算上一期中，是否有空频次的情况
		
		//计算最后一期中包含的高频号、中频号、低频号
		int lastHigh=0, lastMedium=0, lastLow=0;
		Ssq_drawing_history last = lasts.get(0);
		for(int i=1;i<7;i++)
		{
			if(high.contains(last.getItem(i)))
			{
				lastHigh++;
			}else if(medium.contains(last.getItem(i)))
			{
				lastMedium++;
			}else{
				lastLow++;
			}
		}
		//发现上一期有某个频次的个数为0，则下期可能强势出现，所以推荐该频次号球
		if(lastHigh==0){
			result.addAll(high);
		}
		if(lastMedium==0){
			result.addAll(medium);
		}
		if(lastLow==0){
			result.addAll(low);
		}
		
		return result;
	}
}
