package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKnife12 implements Forecast {

	
	/**
	 * 12、用上期蓝号加6等于的数就是下期蓝号要杀的尾数。例如：第29期蓝号开13，用13+6=19，绝杀9尾，
	 * 结果第30期开07;再用07+6=13，绝杀03和13，结果下期开01;再继续用01+6=7，绝杀7，结果下期蓝号开06，
	 * 例子不胜枚举。
	 */
	public List<Integer> forecast(int term)
	{
		int num = DrawingHistoryUtil.getLast10(term).get(0).getItem(7) + 6;
		if(num>9)
			num = num%10;
		
		int num2 = num+10;
		List<Integer> knife = new ArrayList<Integer>();
		if(num2<=16){
			knife.add(num2);
		}
		if(num>0){
			knife.add(num);
		}
		return knife;
	}
	
}
