package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKnife01 implements Forecast {


	/**
	 * 1、用15减去上期蓝球号码，得出的数就是下期要杀的蓝号尾数。
	 * 例如：今年第13期双色球蓝号开出：09，用15-09=06，绝杀蓝号6尾的06和16两个号码，结果第14期开蓝号02，
	 * 杀号成功!再用 15-2=13，杀掉3尾的03和13，结果第15期开05，杀号又正确!我们再用15-5=10，杀0尾，
	 * 结果第16期开03，我们又杀对蓝号。
	 */
	public List<Integer> forecast(int term)
	{
		int blue = Math.abs(15 - DrawingHistoryUtil.getLast10(term).get(0).getItem(7));
		int num = blue%10;
		int num2 = num+10;
		
		List<Integer> knife = new ArrayList<Integer>();
		if(num2<=16){
			knife.add(num2);
		}
		if(num>0){
			knife.add(num);
		}
		return knife;
	}
}
