package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

/**
 * 根据五行与数字的关系，把尾数为12450的这五个尾数分成一组，把36789五个尾 数分成一组，然后请大家看看蓝球开出的号码，
 * 相对应在那一组，你就会发现，近期开出蓝球尾数据为36789的这一组蓝球号最多连续开出两期后就断开，然后开出12450这一组。
 * 这样，我们就可以在连续两期开出36789后，判定下一期杀掉36789的尾号的蓝球。
 * @author 学明
 *
 */
public class PentaKill implements Forecast {

	@Override
	public List<Integer> forecast(int term) {
		int blue1 = DrawingHistoryUtil.getLast10(term).get(0).getItem(7);
		int blue2 = DrawingHistoryUtil.getLast10(term).get(1).getItem(7);
		
		int tail1 = blue1%10;
		int tail2 = blue2%10;
		
		List<Integer> penta = new ArrayList<Integer>();
		if(is36789(tail1) && is36789(tail2)){
			penta.add(3);
			penta.add(13);
			penta.add(6);
			penta.add(16);
			penta.add(7);
			penta.add(8);
			penta.add(9);
		}
		return penta;
	}

	private boolean is36789(int tail) {
		if(tail==3 || tail>5)
			return true;
		return false;
	}

}
