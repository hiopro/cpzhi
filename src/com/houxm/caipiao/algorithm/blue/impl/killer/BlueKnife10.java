package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKnife10 implements Forecast {

	/**
	 * 10、用上期蓝号加7或减7，注意蓝号大于14则减7，小于14则加7，得出的数即为下期要杀的尾数。
	 * 例如：今年第6期开02，用2+7=9，杀9尾，结果第7期开15，完全正确!再用15-7=8，杀掉8尾，结果下期开02，
	 * 我们再用2+7=9，杀9尾，结果第9期开02。
	 */
	public List<Integer> forecast(int term)
	{
		int num = 0;
		int blue = DrawingHistoryUtil.getLast10(term).get(0).getItem(7);
		if(blue>14)
			num = blue - 7;
		else
			num = blue + 7;
		
		if(num>9)
			num = num%10;
		
		int num2 = num+10;
		
		List<Integer> knife = new ArrayList<Integer>();
		if(num2<=16){
			knife.add(num2);
		}
		if(num>0){
			knife.add(num);
		}
		return knife;
	}
	
	
}
