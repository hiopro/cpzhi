package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKnife03 implements Forecast {
	
	/**
	 * 3、用21减上期蓝号得出的数就是下期要杀的尾数。例如：双色球第20期蓝号开：13，用21-13=08，杀掉08，
	 * 结果第21期开：09;再用21-09=12，杀2尾，结果第22期开08;再用21-08=13，杀3尾，结果第23期开：08，
	 * 杀号正确。
	 */
	public List<Integer> forecast(int term)
	{
		int blue = 21 - DrawingHistoryUtil.getLast10(term).get(0).getItem(7);
		int num = blue%10;
		int num2 = num+10;
		List<Integer> knife = new ArrayList<Integer>();
		if(num2<=16){
			knife.add(num2);
		}
		if(num>0){
			knife.add(num);
		}
		return knife;
	}
}
