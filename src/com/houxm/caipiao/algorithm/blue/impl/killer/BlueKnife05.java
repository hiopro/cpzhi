package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKnife05 implements Forecast {

	
	/**
	 * 5、用上两期蓝号的尾和头相加的数即为下期要杀的尾数。例如：第21期开09，第22期开08，用9+0=09，
	 * 杀9尾，结果23期开08;再用8+0=08，杀8尾，结果第24期开12;再用8+1=9，杀掉9尾，结果第25期开11。
	 */
	public List<Integer> forecast(int term)
	{
		int blue1 = DrawingHistoryUtil.getLast10(term).get(0).getItem(7);
		int blue2 = DrawingHistoryUtil.getLast10(term).get(1).getItem(7);
		int tmp1 = blue1>9?blue1/10:0;
		int tmp2 = blue2%10;
		int num = tmp1+tmp2;
		if(num>9)
			num = num%10;
		
		int num2 = num+10;
		
		List<Integer> knife = new ArrayList<Integer>();
		if(num2<=16){
			knife.add(num2);
		}
		if(num>0){
			knife.add(num);
		}
		return knife;
	}
	
}
