package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKiller09 implements Forecast {
	
	/**
	 * 当期最大红球号码与最小号码的差，差数大于16时，减去最大蓝号16所得到的结果为下期需要排除的蓝号。
	 * 例：2010108期红球号码：2、4、8、20、23、24，最大红球号码与最小红球号码的差为（24-2=22，22-16=6），号码06为2010109期需要排除的蓝球号码。结果正确。
	 */
	public List<Integer> forecast(int term)
	{
		int num = DrawingHistoryUtil.getLast10(term).get(0).getItem(6) - DrawingHistoryUtil.getLast10(term).get(0).getItem(1);
		if(num>16)
			num = num%16;
		if(num==0)
			num = 16;
		List<Integer> killer = new ArrayList<Integer>();
		killer.add(num);
		return killer;
	}
	
}
