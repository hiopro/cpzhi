package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKnife11 implements Forecast {
	
	/**
	 * 11、用上期蓝号加2得出的数即为下期要杀的蓝号尾数。例如：第29期开13，用13+2=15，杀掉5尾，结果下期开07;
	 * 再用07+2=9，杀掉9尾，结果第31期开01;我们再用1+2=3，绝杀3尾的03、13，结果第32期开出06。
	 */
	public List<Integer> forecast(int term)
	{
		int num = DrawingHistoryUtil.getLast10(term).get(0).getItem(7) + 2;
		if(num>9)
			num = num%10;
		
		int num2 = num+10;
		List<Integer> knife = new ArrayList<Integer>();
		if(num2<=16){
			knife.add(num2);
		}
		if(num>0){
			knife.add(num);
		}
		return knife;
	}
	
}
