package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKnife07 implements Forecast {
	
	/**
	 * 7、用上期蓝号尾与隔一期蓝号尾相加得出的数即为下期要杀的尾数。 例如：第28期开10，
	 * 与隔一期即26期的16相加，即为0+6=6尾，杀06和16，结果第29期开13;再用29期的13与27期的06
	 * 相加得出09尾，杀9尾，结果第30期开出：07。
	 */
	public List<Integer> forecast(int term)
	{
		int blue1 = DrawingHistoryUtil.getLast10(term).get(0).getItem(7);
		int blue2 = DrawingHistoryUtil.getLast10(term).get(2).getItem(7);
		int num = blue1%10 + (blue2%10);
		
		if(num>9)
			num = num%10;
		
		int num2 = num+10;
		
		List<Integer> knife = new ArrayList<Integer>();
		if(num2<=16){
			knife.add(num2);
		}
		if(num>0){
			knife.add(num);
		}
		return knife;
	}
	
}
