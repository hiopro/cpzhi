package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKnife04 implements Forecast {
	/**
	 * 4、用上两期蓝号的头和尾相加的数即为下期要杀的蓝号尾数。例如：第18期开15，第19期开04，两期的头尾
	 * 相加即1+4=5，杀掉5尾(05、15)，结果第20期开13，杀号成功!再用0+3=3，杀掉3尾，结果第21期开09;
	 * 再用1+9=10，杀掉0尾，结果第22期蓝号开出：08。
	 */
	public List<Integer> forecast(int term)
	{
		int blue1 = DrawingHistoryUtil.getLast10(term).get(0).getItem(7);
		int blue2 = DrawingHistoryUtil.getLast10(term).get(1).getItem(7);
		int tmp1 = blue1%10;
		int tmp2 = blue2>9?blue2/10:0;
		int num = tmp1+tmp2;
		if(num>9)
			num = num%10;
		
		int num2 = num+10;
		
		List<Integer> knife = new ArrayList<Integer>();
		if(num2<=16){
			knife.add(num2);
		}
		if(num>0){
			knife.add(num);
		}
		return knife;
	}
	
}
