package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKiller02 implements Forecast {
	
	

	/**
	 * 隔两期蓝号进行相减（大号—小号），二者之间的差为下期排除的蓝号。
	 * 例：2010105期蓝号11，2010108期蓝号07，大号—小号（11-7=4），号码04为2010109期需要排除的蓝球号码。结果正确。
	 */
	public List<Integer> forecast(int term)
	{
		int num = Math.abs(DrawingHistoryUtil.getLast10(term).get(0).getItem(7)-
				DrawingHistoryUtil.getLast10(term).get(3).getItem(7));
		List<Integer> killer = new ArrayList<Integer>();
		if(num>0)
			killer.add(num);
		return killer;
	}
	
}
