package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKiller05 implements Forecast {
	
	
	/**
	 * 连续两期蓝号进行相加取其个位号码，个位号码即为下期所要排除的蓝球小号码。蓝球小号码范围为1至10。
	 * 例：2010107期蓝号16，2010108期蓝号07，二者之和（16+7=23）的个位号码03，号码03为2010109期需要排除的蓝球号码。结果正确。
	 * 
	 * 利用当期期数尾排除当期的同尾蓝号。
	 * 每期开奖的最大红球号码减去当期蓝号的差为下期所要排除的蓝球号码。
	 * 每期开奖的红球由小到大依次相加，其相加结果以最接近最大蓝号为界线，所得到的和数为下期需要排除的蓝号。
	 * 当期最大红球号码与最小号码的差，差数大于16时，减去最大蓝号16所得到的结果为下期需要排除的蓝号。
	 * 排除同期尾出现过的蓝号。
	 */
	public List<Integer> forecast(int term)
	{
		int num = DrawingHistoryUtil.getLast10(term).get(0).getItem(7) + DrawingHistoryUtil.getLast10(term).get(1).getItem(7);
		num = num%10;
		List<Integer> killer = new ArrayList<Integer>();
		killer.add(num);
		return killer;
	}
	
}
