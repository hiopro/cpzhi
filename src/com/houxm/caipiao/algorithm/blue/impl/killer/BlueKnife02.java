package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKnife02 implements Forecast {

	/**
	 * 2、用19减上期蓝号得出的数即为下期要杀的尾数。例如：今年双色球第1期蓝号开04，用19-04=15，
	 * 绝杀蓝号5尾的05、15两个号码，结果2期开蓝号：14，杀号成功!我们再用 19-14=05，杀掉05、15，
	 * 结果双色球第3期蓝号开02，杀号又成功!我们一鼓作气，再用19-02=17，杀掉07，结果第4期蓝球号码开 03。 
	 */
	public List<Integer> forecast(int term)
	{
		int blue = 19 - DrawingHistoryUtil.getLast10(term).get(0).getItem(7);
		int num = blue%10;
		int num2 = num+10;
		
		List<Integer> knife = new ArrayList<Integer>();
		if(num2<=16){
			knife.add(num2);
		}
		if(num>0){
			knife.add(num);
		}
		return knife;
	}
}
