package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;

public class BlueKiller06 implements Forecast {
	
	/**
	 * 利用当期期数尾排除当期的同尾蓝号。
	 * 例：2010109期的期尾号9，号码09为2010109期需要排除的蓝球号码。结果正确。
	 */
	public List<Integer> forecast(int term)
	{
		int num = term%10;
		List<Integer> killer = new ArrayList<Integer>();
		if(num==0)
			num = 10;
		killer.add(num);
		return killer;
	}
	
}
