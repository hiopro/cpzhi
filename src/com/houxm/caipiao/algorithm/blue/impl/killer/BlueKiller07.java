package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKiller07 implements Forecast {
	
	
	/**
	 * 每期开奖的最大红球号码减去当期蓝号的差为下期所要排除的蓝球号码。
	 * 例：2010108期蓝号07，最大红球号码24，最大红球号码减去当期蓝号（24-7=17，17-16=1）的差为1，号码01为2010109期需要排除的蓝球号码。结果正确。
	 */
	public List<Integer> forecast(int term)
	{
		int num = DrawingHistoryUtil.getLast10(term).get(0).getItem(6) - DrawingHistoryUtil.getLast10(term).get(0).getItem(7);
		if(num>16)
			num = num%16;
		if(num==0)
			num = 16;
		List<Integer> killer = new ArrayList<Integer>();
		killer.add(num);
		return killer;
	}
	
}
