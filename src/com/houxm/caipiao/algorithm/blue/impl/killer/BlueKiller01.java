package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKiller01 implements Forecast {
	
	

	/**
	 * 连续两期蓝号进行相减（大号—小号），二者之间的差为下期排除的蓝号。
	 * 例：2010107期蓝号16，2010108期蓝号07，大号—小号（16-7=9），号码09为2010109期需要排除的蓝球号码。结果正确。
	 * @return
	 */
	public List<Integer> forecast(int term)
	{
		int num = Math.abs(DrawingHistoryUtil.getLast10(term).get(0).getItem(7)-
				DrawingHistoryUtil.getLast10(term).get(1).getItem(7));
		List<Integer> killer = new ArrayList<Integer>();
		if(num>0)
			killer.add(num);
		return killer;
	}
	
}
