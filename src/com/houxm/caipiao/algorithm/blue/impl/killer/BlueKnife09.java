package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKnife09 implements Forecast {
	
	/**
	 * 9、用上期蓝号尾乘4得出的数即是下期要杀的尾数。例如：第29期开13，用3×4=12，
	 * 绝杀2尾，结果第30期开07;再用7×4=28，绝杀8尾，结果第31期开01;再用1×4=4，绝杀4尾，结果第32期开06。 
	 */
	public List<Integer> forecast(int term)
	{
		int num = DrawingHistoryUtil.getLast10(term).get(0).getItem(7)*4;
		if(num>9)
			num = num%10;
		
		int num2 = num+10;
		
		List<Integer> knife = new ArrayList<Integer>();
		if(num2<=16){
			knife.add(num2);
		}
		if(num>0){
			knife.add(num);
		}
		return knife;
	}
	
}
