package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKiller08 implements Forecast {
	
	
	/**
	 * 每期开奖的红球由小到大依次相加，其相加结果以最接近最大蓝号为界线，所得到的和数为下期需要排除的蓝号。
	 * 例：2010108期红球号码：2、4、8、20、23、24，由小到大依次相加，其相加结果最接近16的号码（2+4+8=14）为14，号码14为2010109期需要排除的蓝球号码。结果正确。
	 */
	public List<Integer> forecast(int term)
	{
		int num = 0;
		int[] temp = new int[6];
		temp[0] = DrawingHistoryUtil.getLast10(term).get(0).getItem(1);
		temp[1] = DrawingHistoryUtil.getLast10(term).get(0).getItem(1) + DrawingHistoryUtil.getLast10(term).get(0).getItem(2);
		temp[2] = DrawingHistoryUtil.getLast10(term).get(0).getItem(1) + DrawingHistoryUtil.getLast10(term).get(0).getItem(2)
				+ DrawingHistoryUtil.getLast10(term).get(0).getItem(3);
		temp[3] = DrawingHistoryUtil.getLast10(term).get(0).getItem(1) + DrawingHistoryUtil.getLast10(term).get(0).getItem(2)
				+ DrawingHistoryUtil.getLast10(term).get(0).getItem(3) + DrawingHistoryUtil.getLast10(term).get(0).getItem(4);
		temp[4] = DrawingHistoryUtil.getLast10(term).get(0).getItem(1) + DrawingHistoryUtil.getLast10(term).get(0).getItem(2)
				+ DrawingHistoryUtil.getLast10(term).get(0).getItem(3) + DrawingHistoryUtil.getLast10(term).get(0).getItem(4)
				+ DrawingHistoryUtil.getLast10(term).get(0).getItem(5);
		temp[5] = DrawingHistoryUtil.getLast10(term).get(0).getItem(1) + DrawingHistoryUtil.getLast10(term).get(0).getItem(2)
				+ DrawingHistoryUtil.getLast10(term).get(0).getItem(3) + DrawingHistoryUtil.getLast10(term).get(0).getItem(4)
				+ DrawingHistoryUtil.getLast10(term).get(0).getItem(5) + DrawingHistoryUtil.getLast10(term).get(0).getItem(6);
		for(int i=0;i<6;i++)
		{
			if(temp[i]>16)
			{
				if(i==0)
					num = temp[i]-16;
				else
					num = temp[i-1];
				break;
			}
		}
		
		List<Integer> killer = new ArrayList<Integer>();
		killer.add(num);
		return killer;
	}
	
}
