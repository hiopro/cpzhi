package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKiller10 implements Forecast {
	
	/**
	 * 排除同期尾出现过的蓝号。
	 * 例：2010099期篮球号码06，则号码06为2010109期需要排除的蓝球号码。结果正确。
	 */
	public List<Integer> forecast(int term)
	{
		int num = DrawingHistoryUtil.getLast10(term).get(9).getItem(7);
		List<Integer> killer = new ArrayList<Integer>();
		killer.add(num);
		return killer;
	}
}
