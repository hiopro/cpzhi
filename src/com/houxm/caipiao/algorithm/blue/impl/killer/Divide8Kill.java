package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;
/**
 * 把双色球蓝球除以8的余数分类, 把余数得 2、3、4、5 四个余数的蓝球号归为一类，余数得2345的为一半，余数得0167的为另一半，
 * 大家来看看最近的规律，从2013073期开始到2013148期的开号规律如下，余数为2345这一半蓝号最多连续开出两期，
 * 当连开两期后第三期就会断开，此规律可以杀掉一半的蓝球，结合其它尾数规律等可以一到三个蓝球围住开奖号
 * @author 学明
 *
 */
public class Divide8Kill implements Forecast {

	@Override
	public List<Integer> forecast(int term) {
		int blue1 = DrawingHistoryUtil.getLast10(term).get(0).getItem(7);
		int blue2 = DrawingHistoryUtil.getLast10(term).get(1).getItem(7);
		
		int tail1 = blue1%8;
		int tail2 = blue2%8;
		
		List<Integer> divide8 = new ArrayList<Integer>();
		if(is2345(tail1) && is2345(tail2)){
			divide8.add(2);
			divide8.add(3);
			divide8.add(4);
			divide8.add(5);
			divide8.add(7);
			divide8.add(10);
			divide8.add(11);
			divide8.add(12);
			divide8.add(13);
		}
		return divide8;
	}

	private boolean is2345(int tail) {
		if(tail>1 && tail<6)
			return true;
		return false;
	}
}
