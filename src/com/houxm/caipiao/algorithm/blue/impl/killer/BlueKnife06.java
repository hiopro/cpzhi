package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKnife06 implements Forecast {

	
	/**
	 * 6、用上二期蓝号尾相加得出的数就是下期要杀的尾数。例如：第23期蓝号开08，
	 * 第24期开12，8+2=10，杀0尾，结果下期开11;再用2+1=3，杀3尾，结果第26期开16。
	 */
	public List<Integer> forecast(int term)
	{
		int blue1 = DrawingHistoryUtil.getLast10(term).get(0).getItem(7);
		int blue2 = DrawingHistoryUtil.getLast10(term).get(1).getItem(7);
		int num = blue1%10 + (blue2%10);
		
		if(num>9)
			num = num%10;
		
		int num2 = num+10;
		
		List<Integer> knife = new ArrayList<Integer>();
		if(num2<=16){
			knife.add(num2);
		}
		if(num>0){
			knife.add(num);
		}
		return knife;
	}
	
}
