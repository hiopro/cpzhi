package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKiller04 implements Forecast {
	

	/**
	 * 两期蓝号进行相加，二者之和（如果大于16则减去16）为下期所要排除的蓝号。
	 * 例：2010107期蓝号16，2010108期蓝号07，二者之和（16+7=23）大于16，将和值减去16（23-16=7），号码07为2010109期需要排除的蓝球号码。结果正确。
	 */
	public List<Integer> forecast(int term)
	{
		int num = DrawingHistoryUtil.getLast10(term).get(0).getItem(7) + DrawingHistoryUtil.getLast10(term).get(4).getItem(7);
		if(num>16)
			num = num-16;
		List<Integer> killer = new ArrayList<Integer>();
		if(num>0)
			killer.add(num);
		return killer;
	}
	
}
