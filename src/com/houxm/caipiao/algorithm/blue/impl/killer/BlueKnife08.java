package com.houxm.caipiao.algorithm.blue.impl.killer;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;

public class BlueKnife08 implements Forecast {

	
	/**
	 * 8、用上期蓝号乘以2得出的数即为下期要杀的尾数。例如：第29期开出：13，用13×2=6，
	 * 绝杀6尾，结果第30期开07;再用7×2=14，绝杀04和14，结果第31期开01。 
	 */
	public List<Integer> forecast(int term)
	{
		int num = DrawingHistoryUtil.getLast10(term).get(0).getItem(7)*2;
		if(num>9)
			num = num%10;
		
		int num2 = num+10;
		List<Integer> knife = new ArrayList<Integer>();
		if(num2<=16){
			knife.add(num2);
		}
		if(num>0){
			knife.add(num);
		}
		return knife;
	}
	
	
}
