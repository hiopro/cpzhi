package com.houxm.caipiao.algorithm.blue.impl;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.model.Ssq_drawing_history;

/**
 * 双色球蓝球选号法 准确率99% 
 
    本人观察双色球的蓝球出球现象，并长时间摸索其规律。时间长了，也就容易看出其中的一些带规律的东西。我发现蓝球出球有一个相当有规律的东西，就是当期将要出现的蓝球，与其已出的前三期有非常牢固的联系；为什么这样说？因为我采用将前三期的蓝球相加或相减后得数的尾数和尾数两边的数，就是本期将要出现的蓝球号，其准确率相当高，在99%以上。也就说它们之间的相关係数接近1的正确度。 
     为了使计算出来数与其两边的数区别开来，我们把前三期相加相减得出来的尾数叫做正选数，把计算出尾数的两边的数称做主选数。根据我的观察：正选数一般每隔四到至六天出一次，面主选数则出数非常多，频率相当高。 
      比如2006085期的蓝球08.其前三期的蓝球是：12.10.09.把它们相加或相减可得到以下几个正选号：12+10+09=1；12-10+09=11；12+10-09=3；09-（12-10）=7. 
      根据我的长期观察分析：①正选号一般要间隔六至七天才能出一次（也有连出两次的，但这种情况很少），也就说这些正选号，出现过后四至六天如果出的走势很明显，那么就可以考虑计算出来的正选号中出球，就得要防其出球。 
      规律性最强的是：②除了正选号隔时出球外，所有蓝球就是在正选号两边的数中出球（我们把它定为主选号：比如1两边的16.02；11两边的10.12；3两边的02.04；7两边的06.08），本期中出08。而决不会出差错。这是我统计了近六百期的蓝球出球现象，总结出来的蓝球出球规律。 
       我用这两条规律统计了近三年的蓝球资料，没有一期不准。你们可以去验证，期期准确！！！除了正主选号能出蓝球，正主选号外的球是在本期百分之百出不来的，也就是说，其余各号的球均可杀掉。 
       运用这种方法，有两大难点：一是如何确定是正选号出球还是主选号出球有很大的难度；二是如何在正选号或主选号中定准出球的号，难度是很大的。 
       其实，用这个方法把蓝球的出球号定在这几个数后，再来更确切到某个数，是很容易的事了。但这也是需要有很强的分析和判断力，才能做到更高的准确度。我是根据自己的看法，将这期的蓝球定在08.10.12三个号的范围内，结果出了08。你们去看看我的2006085期蓝球的预测。我是决不会在这些正、主选号之外去定蓝的。 
       这对才入门的彩友来说，确是不容易的。但对于对双色球的蓝球有一定认识和研究的彩友来，利用这个方法，就会得心应手地准确定出蓝球号的。而且一旦熟练后，就可以做到蓝，还能连预测出两期以上的蓝球来！！！ 
      我是没有多的时间来看号，现在只能做到计算出正选号和推算出主选号，并能初略地推荐几个号，有时还有错的。我希望这种方法还可推广应用到红球和3D及排列3、5、7中，听说有人用种方法预测3D成绩还很不错，大家研究一下。
     祝各位网友中奖！！！
 * @author 学明
 *
 */
public class JustChoose implements Forecast {

	
	@Override
	public List<Integer> forecast(int term) {
		List<Integer> just = new ArrayList<Integer>();
		String sql = "select * from ssq_drawing_history where term<? order by term desc limit 3";
		List<Ssq_drawing_history> last3 = Ssq_drawing_history.me.find(sql, term);
		int a = last3.get(0).getItem(7);
		int b = last3.get(1).getItem(7);
		int c = last3.get(2).getItem(7);
		int temp;
		
		/**
		 * 公式一共有8个，如下：
		 * a+b+c、a+b-c、a-b+c、a-b-c、b+c-a、b-c-a、c-b-a、-a-b-c等8个公式，
		 * 但我们会忽略结果中的负值，所以取a+b+c、a+b-c、a-b+c、a-b-c，并取结果的绝对值即可。
		 * 本例中，>16的数字，直接取其余数
		 */
		temp = a+b+c;
		if(temp>16)
		{
			temp = temp%10;
		}
		if(!just.contains(temp))
			just.add(temp);
		
		temp = Math.abs(a+b-c);
		if(temp>16)
		{
			temp = temp%10;
		}
		if(!just.contains(temp))
			just.add(temp);
		
		temp = Math.abs(a-b+c);
		if(temp>16)
		{
			temp = temp%10;
		}
		if(!just.contains(temp))
			just.add(temp);
		
		temp = Math.abs(a-b-c);
		if(temp>16)
		{
			temp = temp%10;
		}
		if(!just.contains(temp))
			just.add(temp);
		
		//删除list中的0
		//获得正选号码
		just.remove(new Integer(0));
		
		return just;
	}
}
