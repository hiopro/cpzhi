/**
 * 旋转矩阵获取聪明组合。
 * @author 学明
 *
 */
package com.houxm.caipiao.algorithm.matrix;

public class RotationMatrix65 extends RotationMatrix {

	@Override
	public int getMethodId() {
		return 1;
	}
}
