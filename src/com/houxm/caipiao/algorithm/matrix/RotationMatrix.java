package com.houxm.caipiao.algorithm.matrix;

import java.util.ArrayList;
import java.util.List;

import com.houxm.caipiao.model.Ssq_matrix_data;
import com.houxm.caipiao.model.Ssq_matrix_method_forecast;

public abstract class RotationMatrix {

	/**
	 * 获取当前方法的id
	 * @return
	 */
	public abstract int getMethodId();
	
	/**
	 * 根据参数balls的个数，使用旋转矩阵获取红球的聪明组合。
	 * 
	 * @param redOdds
	 * @param dao 
	 * @return
	 */
	public List<Ssq_matrix_method_forecast> rotation(int[] reds, int term) {
		// 存放聪明组合的list
		List<Ssq_matrix_method_forecast> forecasts = new ArrayList<Ssq_matrix_method_forecast>();
		// 存放旋转矩阵的list
		List<Ssq_matrix_data> rotationMatrix = null;
		int num = reds.length;
		if (reds.length > 7 && reds.length < 21) {
			rotationMatrix = getRotationMatrix(this.getMethodId(), num);
			// 根据旋转矩阵，获取双色球的聪明组合
			for (Ssq_matrix_data data : rotationMatrix) {
				Ssq_matrix_method_forecast fore = new Ssq_matrix_method_forecast();
				//因为旋转矩阵是从1到9中选择矩阵组合，而redOdds是从0开始的数组，所以需要从data.getBall(?)-1 开始取球的号码，但是按照redOdds的下标需要-1后获得。
				
				int[] ball = new int[6];
				ball[0] = reds[data.getBall(1)-1];
				ball[1] = reds[data.getBall(2)-1];
				ball[2] = reds[data.getBall(3)-1];
				ball[3] = reds[data.getBall(4)-1];
				ball[4] = reds[data.getBall(5)-1];
				ball[5] = reds[data.getBall(6)-1];
				
				int[] ball2 = bubbleSort(ball);
				
				fore.set("red1", ball2[0]);
				fore.set("red2", ball2[1]);
				fore.set("red3", ball2[2]);
				fore.set("red4", ball2[3]);
				fore.set("red5", ball2[4]);
				fore.set("red6", ball2[5]);
				fore.set("term", term);
				fore.set("methodId", data.getMethodId());
				fore.save();
				forecasts.add(fore);
			}
		}
		return forecasts;
	}

	/**
	 * 冒泡排序
	 * @param ball
	 * @return
	 */
	private int[] bubbleSort(int[] ball) {
		for(int i=0;i<ball.length-1;i++){
			for(int j=i+1;j<ball.length;j++){
				if(ball[i]>ball[j]){
					int tmp = ball[i];
					ball[i] = ball[j];
					ball[j] = tmp;
				}
			}
		}
		return ball;
	}

	/**
	 * 根据参数num的值，获取相应的旋转矩阵
	 * @param num
	 * @param dao 
	 * @return
	 */
	public static List<Ssq_matrix_data> getRotationMatrix(int methodId, int num) {
		String sql = "SELECT * FROM ssq_matrix_data where methodId=? and num=?";
		List<Ssq_matrix_data> list = Ssq_matrix_data.me.find(sql, methodId, num);
		return list;
	}
}
