package com.houxm.caipiao.algorithm;

import java.util.List;

public interface Forecast {

	/**
	 * 预测某一期次的出球或杀球情况
	 * @param term
	 * @return
	 */
	public List<Integer> forecast(int term);
}
