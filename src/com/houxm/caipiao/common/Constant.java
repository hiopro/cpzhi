package com.houxm.caipiao.common;


/**
 * 全局静态变量
 * @author 学明
 *
 */
public class Constant {

	
	public static final String BLANK = "";
	
	/**
	 * 计算预测成功率的基础开奖期次
	 */
	public static int TERM_TIMES = 100;
	
	/**
	 * 基础积分
	 */
	public static int BASE_SCORE = 100;
	
	/**
	 * good预测方法
	 */
	public static String GOOD_METHOD = "good";
	
	/**
	 * bad预测方法
	 */
	public static String BAD_METHOD = "bad";
	
	public static int RED_RECOMMEND_INDEX = 15;
	
	public static int BLUE_RECOMMEND_INDEX = 6;
	
	public static float SCORE_LIMIT = 61.8f;
}
