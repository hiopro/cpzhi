package com.houxm.caipiao.common;

import com.houxm.caipiao.blog.Blog;
import com.houxm.caipiao.blog.BlogController;
import com.houxm.caipiao.controller.IndexController;
import com.houxm.caipiao.model.Ssq_blue_method;
import com.houxm.caipiao.model.Ssq_blue_method_analysis;
import com.houxm.caipiao.model.Ssq_blue_method_forecast;
import com.houxm.caipiao.model.Ssq_blue_plan;
import com.houxm.caipiao.model.Ssq_blue_plan_analysis;
import com.houxm.caipiao.model.Ssq_blue_plan_forecast;
import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_drawing_reward;
import com.houxm.caipiao.model.Ssq_filter_method;
import com.houxm.caipiao.model.Ssq_forecast_catalog;
import com.houxm.caipiao.model.Ssq_group_plan;
import com.houxm.caipiao.model.Ssq_group_plan_analysis;
import com.houxm.caipiao.model.Ssq_group_plan_forecast;
import com.houxm.caipiao.model.Ssq_matrix_data;
import com.houxm.caipiao.model.Ssq_matrix_method;
import com.houxm.caipiao.model.Ssq_matrix_method_forecast;
import com.houxm.caipiao.model.Ssq_red_method;
import com.houxm.caipiao.model.Ssq_red_method_analysis;
import com.houxm.caipiao.model.Ssq_red_method_forecast;
import com.houxm.caipiao.model.Ssq_red_plan;
import com.houxm.caipiao.model.Ssq_red_plan_analysis;
import com.houxm.caipiao.model.Ssq_red_plan_forecast;
import com.houxm.caipiao.plugin.scheduler.Cron4jPlugin;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.redis.RedisPlugin;

/**
 * API引导式配置
 */
public class CommonConfig extends JFinalConfig {
	
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		// 加载少量必要配置，随后可用PropKit.get(...)获取值
		PropKit.use("a_little_config.txt");
		me.setDevMode(PropKit.getBoolean("devMode", false));
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add("/", IndexController.class, "/index");	// 第三个参数为该Controller的视图存放路径
		me.add("/blog", BlogController.class);			// 第三个参数省略时默认与第一个参数值相同，在此即为 "/blog"
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		// 配置C3p0数据库连接池插件
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"),
				PropKit.get("user"),
				PropKit.get("password").trim(),
				PropKit.get("driverClass"),
				PropKit.getInt("maxPoolSize"),
				PropKit.getInt("minPoolSize"),
				PropKit.getInt("initialPoolSize"),
				PropKit.getInt("maxIdleTime"),
				PropKit.getInt("acquireIncrement"));
//		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"),
//				PropKit.get("user"),
//				PropKit.get("password").trim());
		me.add(c3p0Plugin);
		
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		
		// 映射ssq_drawing_history 表到 Ssq_drawing_history模型
		arp.addMapping("ssq_drawing_reward", Ssq_drawing_reward.class);
		arp.addMapping("ssq_drawing_history", Ssq_drawing_history.class);
		arp.addMapping("Ssq_filter_method", Ssq_filter_method.class);
		arp.addMapping("ssq_forecast_catalog", Ssq_forecast_catalog.class);
		
		arp.addMapping("ssq_matrix_data", Ssq_matrix_data.class);
		arp.addMapping("ssq_matrix_method", Ssq_matrix_method.class);
		arp.addMapping("ssq_matrix_method_forecast", Ssq_matrix_method_forecast.class);
		
		arp.addMapping("ssq_red_method", Ssq_red_method.class);
		arp.addMapping("ssq_red_method_forecast", Ssq_red_method_forecast.class);
		arp.addMapping("ssq_red_method_analysis", Ssq_red_method_analysis.class);
		arp.addMapping("ssq_red_plan", Ssq_red_plan.class);
		arp.addMapping("ssq_red_plan_forecast", Ssq_red_plan_forecast.class);
		arp.addMapping("ssq_red_plan_analysis", Ssq_red_plan_analysis.class);
		
		arp.addMapping("ssq_blue_method", Ssq_blue_method.class);
		arp.addMapping("ssq_blue_method_forecast", Ssq_blue_method_forecast.class);
		arp.addMapping("ssq_blue_method_analysis", Ssq_blue_method_analysis.class);
		arp.addMapping("ssq_blue_plan", Ssq_blue_plan.class);
		arp.addMapping("ssq_blue_plan_forecast", Ssq_blue_plan_forecast.class);
		arp.addMapping("ssq_blue_plan_analysis", Ssq_blue_plan_analysis.class);
		
		arp.addMapping("ssq_group_plan", Ssq_group_plan.class);
		arp.addMapping("ssq_group_plan_forecast", Ssq_group_plan_forecast.class);
		arp.addMapping("ssq_group_plan_analysis", Ssq_group_plan_analysis.class);
		
		arp.addMapping("blog", Blog.class);
		me.add(arp);
		
		//调度，执行一次调度后，10秒后继续执行下一次的调度
		Cron4jPlugin cp = new Cron4jPlugin("job.properties");
        me.add(cp);
        
        // 用于缓存bbs模块的redis服务
        RedisPlugin redis = new RedisPlugin("kuaidi", "localhost", 6379);
        me.add(redis);
	}
	
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		
	}
	
	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		JFinal.start("WebRoot", 80, "/", 5);
	}
}
