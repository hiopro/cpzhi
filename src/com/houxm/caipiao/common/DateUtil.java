package com.houxm.caipiao.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间工具
 * @author 学明
 *
 */
public class DateUtil {
	public static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat format3 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	public static SimpleDateFormat format2 = new SimpleDateFormat("yyyyMMdd");
	public static SimpleDateFormat format4 = new SimpleDateFormat("yyyy/MM/dd");
	
	public static void main(String[] args) throws Exception {
		System.out.println(format4.parse("2016/1/1"));
		System.out.println(Integer.parseInt("07"));
	}
	public static String getCurrentTime(){
		return format.format(new Date());
	}
	
	public static Date parse(String date){
		if(date==null || date.trim().length()<10)
			return new Date();
		try {
			Date d = null;
			if(date.contains("/"))
				d = format3.parse(date);
			else
				d = format.parse(date);
			return d;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new Date();
	}
	
	public static String getCurrentDay(){
		return format2.format(new Date());
	}
}
