package com.houxm.caipiao.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * mysql> desc ssq_drawing_reward;
+-------------+-------------+------+-----+---------+----------------+
| Field       | Type        | Null | Key | Default | Extra          |
+-------------+-------------+------+-----+---------+----------------+
| id          | int(11)     | NO   | PRI | NULL    | auto_increment |
| term        | int(11)     | NO   |     | NULL    |                |
| lvl1        | float       | YES  |     | NULL    |                |
| lvl1money   | float       | YES  |     | NULL    |                |
| lvl2        | float       | YES  |     | NULL    |                |
| lvl2money   | float       | YES  |     | NULL    |                |
| lvl3        | float       | YES  |     | NULL    |                |
| lvl4        | float       | YES  |     | NULL    |                |
| lvl5        | float       | YES  |     | NULL    |                |
| lvl6        | float       | YES  |     | NULL    |                |
| lotteryDate | date        | YES  |     | NULL    |                |
| lotteryWeek | varchar(45) | YES  |     | NULL    |                |
| saleMoney   | double      | YES  |     | NULL    |                |
| poolMoney   | double      | YES  |     | NULL    |                |
+-------------+-------------+------+-----+---------+----------------+
14 rows in set (0.00 sec)

 * @author 学明
 *
 */
@SuppressWarnings("serial")
public class Ssq_drawing_reward extends Model<Ssq_drawing_reward> {

	public static final Ssq_drawing_reward me = new Ssq_drawing_reward();
}
