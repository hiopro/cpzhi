package com.houxm.caipiao.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * mysql> desc Ssq_red_method_analysis;
+-------------+--------------+------+-----+---------+-------+
| Field       | Type         | Null | Key | Default | Extra |
+-------------+--------------+------+-----+---------+-------+
| id          | int(11)      | NO   | PRI | NULL    |       |
| focastId    | int(11)      | YES  |     | NULL    |       |
| methodId    | int(11)      | YES  | MUL | NULL    |       |
| term        | int(11)      | YES  |     | NULL    |       |
| sucReds     | varchar(200) | YES  |     | NULL    |       |
| totalNum    | int(11)      | YES  |     | NULL    |       |
| sucNum      | int(11)      | YES  |     | NULL    |       |
| suceed      | int(2)       | YES  |     | NULL    |       |
| thisSucRate | float        | YES  |     | NULL    |       |
+-------------+--------------+------+-----+---------+-------+
9 rows in set (0.00 sec)
 * 
 * @author 学明
 *
 */
@SuppressWarnings("serial")
public class Ssq_red_method_analysis extends Model<Ssq_red_method_analysis> {

	public static final Ssq_red_method_analysis me = new Ssq_red_method_analysis();
}
