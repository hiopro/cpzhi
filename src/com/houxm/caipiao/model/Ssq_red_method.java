package com.houxm.caipiao.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * mysql> desc ssq_red_method;
+-----------+---------------+------+-----+---------+----------------+
| Field     | Type          | Null | Key | Default | Extra          |
+-----------+---------------+------+-----+---------+----------------+
| id        | int(11)       | NO   | PRI | NULL    | auto_increment |
| name      | varchar(200)  | YES  |     | NULL    |                |
| title     | varchar(200)  | YES  |     | NULL    |                |
| type      | varchar(45)   | YES  |     | NULL    |                |
| catalogId | int(11)       | YES  |     | NULL    |                |
| javaClass | varchar(200)  | YES  |     | NULL    |                |
| algorithm | varchar(1000) | YES  |     | NULL    |                |
| example   | varchar(1000) | YES  |     | NULL    |                |
| remark    | varchar(1000) | YES  |     | NULL    |                |
+-----------+---------------+------+-----+---------+----------------+
9 rows in set (0.00 sec)
 * @author 学明
 *
 */
@SuppressWarnings("serial")
public class Ssq_red_method extends Model<Ssq_red_method> {

	public static final Ssq_red_method me = new Ssq_red_method();
}
