package com.houxm.caipiao.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * mysql> desc ssq_red_method_forcast;
 * +------------+--------------+------+-----+---------+-------+
 * | Field      | Type         | Null | Key | Default | Extra |
 * +------------+--------------+------+-----+---------+-------+
 * | id         | int(11)      | NO   | PRI | NULL    |       |
 * | methodId   | int(11)      | NO   |     | NULL    |       |
 * | term       | int(11)      | NO   |     | NULL    |       |
 * | reds       | varchar(200) | YES  |     | NULL    |       |
 * | rate       | float        | YES  |     | NULL    |       |
 * | confidence | float        | YES  |     | NULL    |       |
 * +------------+--------------+------+-----+---------+-------+
 * 6 rows in set (0.00 sec)
 * @author 学明
 *
 */
@SuppressWarnings("serial")
public class Ssq_red_method_forecast extends Model<Ssq_red_method_forecast> {

	public static final Ssq_red_method_forecast me = new Ssq_red_method_forecast();
}
