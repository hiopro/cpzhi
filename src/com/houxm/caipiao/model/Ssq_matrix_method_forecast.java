package com.houxm.caipiao.model;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class Ssq_matrix_method_forecast extends Model<Ssq_matrix_method_forecast> {

	public static final Ssq_matrix_method_forecast me = new Ssq_matrix_method_forecast();
	
	public int getBall(int index){
		switch(index){
		case 1:
			return this.getInt("red1");
		case 2:
			return this.getInt("red2");
		case 3:
			return this.getInt("red3");
		case 4:
			return this.getInt("red4");
		case 5:
			return this.getInt("red5");
		case 6:
			return this.getInt("red6");
		}
		return 0;
	}
	
	public int getTerm(){
		return this.getInt("term");
	}
}
