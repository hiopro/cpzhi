package com.houxm.caipiao.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * mysql> desc Ssq_drawing_history;
+-------+---------+------+-----+---------+----------------+
| Field | Type    | Null | Key | Default | Extra          |
+-------+---------+------+-----+---------+----------------+
| id    | int(11) | NO   | PRI | NULL    | auto_increment |
| term  | int(11) | NO   |     | NULL    |                |
| red1  | int(11) | NO   |     | NULL    |                |
| red2  | int(11) | NO   |     | NULL    |                |
| red3  | int(11) | NO   |     | NULL    |                |
| red4  | int(11) | NO   |     | NULL    |                |
| red5  | int(11) | NO   |     | NULL    |                |
| red6  | int(11) | NO   |     | NULL    |                |
| blue  | int(11) | NO   |     | NULL    |                |
+-------+---------+------+-----+---------+----------------+
9 rows in set (0.00 sec)

 * @author 学明
 *
 */
@SuppressWarnings("serial")
public class Ssq_drawing_history extends Model<Ssq_drawing_history> {

	public static final Ssq_drawing_history me = new Ssq_drawing_history();
	
	/**
	 * 根据参数num选择性的获取类中的属性。
	 * 0返回期次；1~6返回红球；7返回蓝球。
	 * @param num
	 * @return
	 */
	public int getItem(int num){
		switch(num)
		{
			case 0:
				return this.getInt("term");
			case 1:
				return this.getInt("red1");
			case 2:
				return this.getInt("red2");
			case 3:
				return this.getInt("red3");
			case 4:
				return this.getInt("red4");
			case 5:
				return this.getInt("red5");
			case 6:
				return this.getInt("red6");
			case 7:
				return this.getInt("blue");
			default:
				return -1;
		}
	}
	
	/**
	 * 判断该红球是否为中奖号码
	 * @param red
	 * @return
	 */
	public boolean isWinningRed(int red){
		for(int i=1;i<7;i++){
			if(red == this.getItem(i)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 判断该蓝球是否为中奖号码
	 * @return
	 */
	public boolean isWinningBlue(int blue){
		if(blue == this.getInt("blue")){
			return true;
		}else
			return false;
	}
}
