package com.houxm.caipiao.model;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class Ssq_blue_plan_forecast extends Model<Ssq_blue_plan_forecast> {

	public static final Ssq_blue_plan_forecast me = new Ssq_blue_plan_forecast();
	
	/**
	 * 显示出球概率大于等于100的红球情况
	 * @return
	 */
	public String showBluesAbove100(){
		String above100 = "";
		String odds = this.getStr("odds");
		String blues = this.getStr("blues");
		String[] rate = odds.split(",");
		String[] blue = blues.split(",");
		int num=0;
		for(int i=0;i<rate.length;i++){
			if(Float.parseFloat(rate[i])<100.0f){
				num = i;
				break;
			}
		}
		
		for(int i=0;i<num;i++){
			if(i==num-1){
				above100 += blue[i];
			}else{
				above100 += blue[i]+",";
			}
		}
		return above100;
	}
	
	/**
	 * 显示出球概率大于等于100的概率情况
	 * @return
	 */
	public String showOddsAbove100(){
		String above100 = "";
		String odds = this.getStr("odds");
		String[] rate = odds.split(",");
		for(int i=0;i<rate.length;i++){
			if(Float.parseFloat(rate[i])<100.0f){
				above100 = above100.substring(0, above100.length());
				break;
			}else{
				above100 += rate[i]+",";
			}
		}
		
		return above100;
	}
	
	/**
	 * 显示出球概率大于等于100的红球情况
	 * @return
	 */
	public String showBluesByGoldenSection(){
		String above100 = "";
		String odds = this.getStr("odds");
		String blues = this.getStr("blues");
		String[] rate = odds.split(",");
		String[] blue = blues.split(",");
		int num=0;
		for(int i=0;i<rate.length;i++){
			if(Float.parseFloat(rate[i])<61.8f){
				num = i;
				break;
			}
		}
		
		for(int i=0;i<num;i++){
			if(i==num-1){
				above100 += blue[i];
			}else{
				above100 += blue[i]+",";
			}
		}
		return above100;
	}
	
	/**
	 * 显示出球概率大于等于100的概率情况
	 * @return
	 */
	public String showOddsByGoldenSection(){
		String above100 = "";
		String odds = this.getStr("odds");
		String[] rate = odds.split(",");
		for(int i=0;i<rate.length;i++){
			if(Float.parseFloat(rate[i])<61.8f){
				above100 = above100.substring(0, above100.length());
				break;
			}else{
				above100 += rate[i]+",";
			}
		}
		
		return above100;
	}
	
	public String getBlues(int num){
		if(num<1 || num>16){
			return null;
		}
		String blues = this.getStr("blues");
		String[] blue = blues.split(",");
		String result = "";
		for(int i=0;i<num;i++){
			result += Integer.parseInt(blue[i]);
			if(i<num-1){
				result += ",";
			}
		}
		return result;
	}
	
	public int[] getBlueInts(int num){
		if(num<1 || num>16){
			return null;
		}
		String blues = this.getStr("blues");
		String[] blue = blues.split(",");
		int[] result = new int[num];
		for(int i=0;i<num;i++){
			result[i] = Integer.parseInt(blue[i]);
		}
		return result;
	}
}
