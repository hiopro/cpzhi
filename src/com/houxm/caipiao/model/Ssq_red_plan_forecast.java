package com.houxm.caipiao.model;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class Ssq_red_plan_forecast extends Model<Ssq_red_plan_forecast> {

	public static final Ssq_red_plan_forecast me = new Ssq_red_plan_forecast();
	
	/**
	 * 显示出球概率大于等于100的红球情况
	 * @return
	 */
	public String showRedsAbove100(){
		String above100 = "";
		String odds = this.getStr("odds");
		String reds = this.getStr("reds");
		String[] rate = odds.split(",");
		String[] red = reds.split(",");
		int num=0;
		for(int i=0;i<rate.length;i++){
			if(Float.parseFloat(rate[i])<100.0f){
				num = i;
				break;
			}
		}
		
		for(int i=0;i<num;i++){
			if(i==num-1){
				above100 += red[i];
			}else{
				above100 += red[i]+",";
			}
		}
		return above100;
	}
	
	/**
	 * 显示出球概率大于等于100的概率情况
	 * @return
	 */
	public String showOddsAbove100(){
		String above100 = "";
		String odds = this.getStr("odds");
		String[] rate = odds.split(",");
		for(int i=0;i<rate.length;i++){
			if(Float.parseFloat(rate[i])<100.0f){
				above100 = above100.substring(0, above100.length());
				break;
			}else{
				above100 += rate[i]+",";
			}
		}
		
		return above100;
	}
	
	public int[] getReds(int num){
		if(num<1 || num>33){
			return null;
		}
		String reds = this.getStr("reds");
		String[] red = reds.split(",");
		int[] result = new int[num];
		for(int i=0;i<num;i++){
			result[i] = Integer.parseInt(red[i]);
		}
		return result;
	}
	
	public float[] getRedOdds(int num){
		if(num<1 || num>33){
			return null;
		}
		String odds = this.getStr("odds");
		String[] od = odds.split(",");
		float[] result = new float[num];
		for(int i=0;i<num;i++){
			result[i] = Float.parseFloat(od[i]);
		}
		return result;
	}
}
