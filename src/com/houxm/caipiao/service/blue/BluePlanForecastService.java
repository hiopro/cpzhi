package com.houxm.caipiao.service.blue;

import java.util.List;

import com.houxm.caipiao.common.Constant;
import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_drawing_reward;
import com.houxm.caipiao.model.Ssq_blue_method;
import com.houxm.caipiao.model.Ssq_blue_method_analysis;
import com.houxm.caipiao.model.Ssq_blue_method_forecast;
import com.houxm.caipiao.model.Ssq_blue_plan;
import com.houxm.caipiao.model.Ssq_blue_plan_analysis;
import com.houxm.caipiao.model.Ssq_blue_plan_forecast;
import com.houxm.caipiao.util.BlueOddsUtil;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class BluePlanForecastService {

	public static void main(String[] args) {
		PropKit.use("a_little_config.txt");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"),
		PropKit.get("user"),
		PropKit.get("password").trim());
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.addMapping("ssq_drawing_reward", Ssq_drawing_reward.class);
		arp.addMapping("ssq_drawing_history", Ssq_drawing_history.class);
		arp.addMapping("ssq_blue_method", Ssq_blue_method.class);
		arp.addMapping("ssq_blue_method_forecast", Ssq_blue_method_forecast.class);
		arp.addMapping("ssq_blue_method_analysis", Ssq_blue_method_analysis.class);
		arp.addMapping("ssq_blue_plan", Ssq_blue_plan.class);
		arp.addMapping("ssq_blue_plan_forecast", Ssq_blue_plan_forecast.class);
		arp.addMapping("ssq_blue_plan_analysis", Ssq_blue_plan_analysis.class);
		arp.start();
		
		Ssq_blue_plan plan = Ssq_blue_plan.me.findFirst("SELECT * FROM ssq_blue_plan where id=1");
		initPlanForecastSince2004(plan);
		System.out.println("done");
	}

	private static void initPlanForecastSince2004(Ssq_blue_plan plan) {
		String sql = "select * from ssq_drawing_history where term>2016014 order by term asc";
		List<Ssq_drawing_history> list = Ssq_drawing_history.me.find(sql);
		int term = 0;
		//预测所有已开奖的期次
		for(int i=0;i<list.size();i++){
			Ssq_drawing_history history = list.get(i);
			term = history.getItem(0);
			forecastByGoldenSection(term, plan);
			BluePlanAnalysisService.analysis(history, plan);
		}
	}
	
	/**
	 * 预测某一期次，某一个方案，过滤小于最小出球积分的所有积分组合后的结果。
	 * @param term
	 * @param plan
	 * @param minRate
	 */
	public static void forecast(int term, Ssq_blue_plan plan, float minRate){
		String mainMethod = plan.getStr("mainMethod");
		String justMethod = plan.getStr("justMethod");
		String[] main = mainMethod.split(",");
		String[] just = justMethod.split(",");
		float[] r1 = castMethods(term, main, minRate);
		float[] r2 = castMethods(term, just, minRate);
		BlueOddsUtil blue = new BlueOddsUtil();
		blue.plus(r1, 2);
		blue.plus(r2);
		
		Ssq_blue_plan_forecast fore = new Ssq_blue_plan_forecast();
		fore.set("term", term);
		fore.set("planId", plan.getInt("id"));
		fore.set("blues", blue.ballToString());
		fore.set("odds", blue.rateToString());
		fore.save();
	}

	/**
	 * 预测某一期次，某一个方案的出球积分的所有积分组合后的结果。注意，本类会自动忽略积分小于黄金分割值61.8的方法。
	 * @param term
	 * @param plan
	 */
	public static void forecastByGoldenSection(int term, Ssq_blue_plan plan){
		String mainMethod = plan.getStr("mainMethod");
		String justMethod = plan.getStr("justMethod");
		String[] main = mainMethod.split(",");
		String[] just = justMethod.split(",");
		float[] r1 = castMethods(term, main, 38.2f);
		float[] r2 = castMethods(term, just, 38.2f);
		BlueOddsUtil blue = new BlueOddsUtil();
		blue.plus(r1, 2);
		blue.plus(r2);
		
		Ssq_blue_plan_forecast fore = new Ssq_blue_plan_forecast();
		fore.set("term", term);
		fore.set("planId", plan.getInt("id"));
		fore.set("blues", blue.ballToString());
		fore.set("odds", blue.rateToString());
		fore.save();
	}
	
	public static float[] castMethods(int term, String[] main, float minRate) {
		float[] result = BlueOddsUtil.buildNewOdds17();
		for(int i=0;main!=null && i<main.length;i++){
			String tmp = main[i];
			int methodId = Integer.parseInt(tmp);
			float[] rate = getRateOfMethodByGoldenSection(term, methodId);
			result = BlueOddsUtil.plus(result, rate);
		}
		return result;
	}

	public static float[] castMethodsByGoldenSection(int term, String[] main) {
		float[] result = BlueOddsUtil.buildNewOdds17();
		for(int i=0;main!=null && i<main.length;i++){
			String tmp = main[i];
			int methodId = Integer.parseInt(tmp);
			float[] rate = getRateOfMethodByGoldenSection(term, methodId);
			result = BlueOddsUtil.plus(result, rate);
		}
		return result;
	}
	
	public static float[] getRateOfMethodByGoldenSection(int term, int methodId){
		return getRateOfMethod(term, methodId, 6.18f);
	}
	
	public static float[] getRateOfMethod(int term, int methodId, float minRate){
		float[] result = BlueOddsUtil.buildNewOdds17();
		String sql = "select * from ssq_blue_method_forecast where term=? and methodId=?";
		Ssq_blue_method_forecast forecast = Ssq_blue_method_forecast.me.findFirst(sql, term, methodId);
		String blues = forecast.getStr("blues");
		float rate = forecast.getFloat("rate");
		if(rate<minRate){
			return result;
		}
		String type = forecast.getStr("type");
		if(Constant.BAD_METHOD.equals(type)){
			rate = rate * -1;
		}
		if(blues!=null && !"".equals(blues)){
			String[] blue = blues.split(",");
			for(int j=0;j<blue.length;j++){
				int ball = Integer.parseInt(blue[j]);
				result[ball] = rate;
			}
		}
		return result;
	}
	
}
