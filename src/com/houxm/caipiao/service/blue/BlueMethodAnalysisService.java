package com.houxm.caipiao.service.blue;

import java.util.List;

import com.houxm.caipiao.common.Constant;
import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_drawing_reward;
import com.houxm.caipiao.model.Ssq_blue_method;
import com.houxm.caipiao.model.Ssq_blue_method_analysis;
import com.houxm.caipiao.model.Ssq_blue_method_forecast;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class BlueMethodAnalysisService {

	public static void main(String[] args) {
		PropKit.use("a_little_config.txt");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"),
		PropKit.get("user"),
		PropKit.get("password").trim());
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.addMapping("ssq_drawing_reward", Ssq_drawing_reward.class);
		arp.addMapping("ssq_drawing_history", Ssq_drawing_history.class);
		arp.addMapping("ssq_blue_method", Ssq_blue_method.class);
		arp.addMapping("ssq_blue_method_forecast", Ssq_blue_method_forecast.class);
		arp.addMapping("ssq_blue_method_analysis", Ssq_blue_method_analysis.class);
		arp.start();
		
		System.out.println("done");
	}

	/**
	 * 分析某一期次的所有执行了预测的方法的准确性。
	 * @param history
	 */
	public static void analysis(Ssq_drawing_history history){
		int term = history.getItem(0);
		String sql = "select * from ssq_blue_method where id in (SELECT methodId FROM ssq_blue_method_forecast where term=?)";
		List<Ssq_blue_method> list = Ssq_blue_method.me.find(sql, term);
		for(Ssq_blue_method method: list){
			analysis(history, method);
		}
	}
	
	/**
	 * 分析某一期次的某一预测方法的准确性。
	 * @param history
	 * @param method
	 */
	public static void analysis(Ssq_drawing_history history,
			Ssq_blue_method method) {
		Ssq_blue_method_analysis analysis = new Ssq_blue_method_analysis();
		int term = history.getItem(0);
		int methodId = method.getInt("id");
		analysis.set("term", term);
		analysis.set("methodId", methodId);
		String sql = "select * from Ssq_blue_method_forecast where term=? and methodId=?";
		Ssq_blue_method_forecast forecast = Ssq_blue_method_forecast.me.findFirst(sql, term, methodId);
		if(forecast!=null){
			analysis.set("forecastId", forecast.getInt("id"));
			String blues = forecast.get("blues");
			if(blues==null||"".equals(blues)){
				analysis.set("totalNum", 0);
				analysis.set("sucNum", 0);
				analysis.set("succeed", 0);
				analysis.set("thisSucRate", 0.0f);
			}else{
				String[] blue = blues.split(",");
				String type = method.getStr("type");
				
				String sucBall = "";
				int sucNum=0;
				int succeed = 0;
				//如果是推荐号球的方法，推荐成功一个号球，则认为推荐成功
				//并记录推荐号球中的中奖号球，和中奖个数
				if(Constant.GOOD_METHOD.equals(type)){
					for(int j=0;j<blue.length;j++){
						int ball = Integer.parseInt(blue[j]);
						if(history.isWinningBlue(ball)){
							sucBall += blue[j]+",";
							sucNum++;
						}
					}
					
					if(sucNum>0)
						succeed = 1;
				}else{
					//如果是杀球的方法，杀掉一个中奖号球，则认为杀球失败
					//并记录杀球的号球中的没有中奖的号球，及其个数
					
					for(int j=0;j<blue.length;j++){
						int ball = Integer.parseInt(blue[j]);
						if(!history.isWinningBlue(ball)){
							sucBall += blue[j]+",";
							sucNum++;
						}
					}
					
					if(sucNum==blue.length){
						succeed = 1;
					}
				}
				
				analysis.set("totalNum", blue.length);
				analysis.set("sucNum", sucNum);
				analysis.set("succeed", succeed);
				analysis.set("thisSucRate", sucNum*1.0f/blue.length);
				analysis.set("sucBlues", sucBall);
			}
		}else{
			return;
		}
		
		analysis.save();
	}

}
