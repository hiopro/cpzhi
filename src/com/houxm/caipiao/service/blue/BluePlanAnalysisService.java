package com.houxm.caipiao.service.blue;

import java.util.List;

import com.houxm.caipiao.common.Constant;
import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_blue_plan;
import com.houxm.caipiao.model.Ssq_blue_plan_analysis;
import com.houxm.caipiao.model.Ssq_blue_plan_forecast;

public class BluePlanAnalysisService {

	/**
	 * 对某一期次的某一计划的红球出球排序进行分析统计出球范围。
	 * @param history
	 * @param plan
	 */
	public static void analysis(Ssq_drawing_history history, Ssq_blue_plan plan) {
		String sql = "select * from ssq_blue_plan_forecast where term=? and planId=?";
		int term = history.getItem(0);
		int planId = plan.getInt("id");
		Ssq_blue_plan_forecast fore = Ssq_blue_plan_forecast.me.findFirst(sql, term, planId);
		String blues = fore.getStr("blues");
		String[] blue = blues.split(",");
		int index=0,sucNum6=0;
		for(int i=0;i<blue.length;i++){
			if(history.isWinningBlue(Integer.parseInt(blue[i]))){
				index = i+1;
				if(i<Constant.BLUE_RECOMMEND_INDEX){
					sucNum6++;
				}
			}
		}
		
		Ssq_blue_plan_analysis analysis = new Ssq_blue_plan_analysis();
		analysis.set("term", term);
		analysis.set("planId", planId);
		analysis.set("factIndex", index);
		analysis.set("sucNum6", sucNum6);
		
		String golden = fore.showBluesByGoldenSection();
		if(golden!=null && !"".equals(golden)){
			String[] above = golden.split(",");
			analysis.set("desiredIndex", above.length);
			int succeed = 0;
			for(int i=0;i<above.length;i++){
				if(history.isWinningBlue(Integer.parseInt(above[i]))){
					succeed = 1;
				}
			}
			
			float thisSucRate = 0.0f;
			thisSucRate = 1.0f/above.length;
			
			analysis.set("succeed", succeed);
			analysis.set("thisSucRate", thisSucRate);
		}else{
			analysis.set("desiredIndex", 0);
			analysis.set("succeed", 0);
			analysis.set("thisSucRate", 0.0f);
		}
		
		String avgSql = "select * from ssq_blue_plan_analysis where planId=? and term<=? and succeed>=0 order by term desc limit 100";
		List<Ssq_blue_plan_analysis> list = Ssq_blue_plan_analysis.me.find(avgSql, planId, term);
		if(list==null || list.size()==0){
			analysis.set("avgRate", analysis.getFloat("thisSucRate"));
			analysis.set("succeedRate", analysis.getInt("succeed")*1.0f);
		}else{
			int total = list.size();
			float rateTotal = 0.0f;
			int succeedTotal = 0, suc6Total100 = 0, suc6Total10 = 0;
			for(int i=0;i<total;i++){
				Ssq_blue_plan_analysis a = list.get(i);
				rateTotal += a.getFloat("thisSucRate");
				succeedTotal += a.getInt("succeed");
				suc6Total100 += a.getInt("sucNum6");
				if(i<10-1){
					suc6Total10 += a.getInt("sucNum6");
				}
			}
			
			//本条要保存的结果分析也加入计算
			rateTotal += analysis.getFloat("thisSucRate");
			succeedTotal += analysis.getInt("succeed");
			suc6Total100 += analysis.getInt("sucNum6");
			suc6Total10 += analysis.getInt("sucNum6");
			total++;
			
			analysis.set("avgRate", rateTotal/total);
			analysis.set("succeedRate", succeedTotal*1.0f/total);
			analysis.set("suc100", suc6Total100);
			analysis.set("suc10", suc6Total10);
		}
		
		analysis.save();
	}

}
