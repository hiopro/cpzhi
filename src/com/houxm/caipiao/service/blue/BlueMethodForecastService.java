package com.houxm.caipiao.service.blue;

import java.util.List;

import com.houxm.caipiao.algorithm.Forecast;
import com.houxm.caipiao.model.Ssq_blue_method;
import com.houxm.caipiao.model.Ssq_blue_method_analysis;
import com.houxm.caipiao.model.Ssq_blue_method_forecast;
import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_drawing_reward;
import com.houxm.caipiao.util.BallUtil;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class BlueMethodForecastService {

	public static void main(String[] args) {
		PropKit.use("a_little_config.txt");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"),
		PropKit.get("user"),
		PropKit.get("password").trim());
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.addMapping("ssq_drawing_reward", Ssq_drawing_reward.class);
		arp.addMapping("ssq_drawing_history", Ssq_drawing_history.class);
		arp.addMapping("ssq_blue_method", Ssq_blue_method.class);
		arp.addMapping("ssq_blue_method_forecast", Ssq_blue_method_forecast.class);
		arp.addMapping("ssq_blue_method_analysis", Ssq_blue_method_analysis.class);
		arp.start();
		
		List<Ssq_blue_method> list = Ssq_blue_method.me.find("select * from ssq_blue_method");
		for(Ssq_blue_method method:list)
			initMethodForecastSince2004(method);
		System.out.println("done");
	}
	
	/**
	 * 预测一个红球预测方法的自2004年以来所有已开奖期次的红球。
	 * 预测一个之后，立马分析其预测的结果。
	 * 双色球从2003开始可以购买，2003年的开奖数据仅作为数据参考，不作为预测范围。
	 * 虽然开奖结果已经有了，但会根据本次结果计算分析该预测方法的准确率。
	 * @param method
	 */
	private static void initMethodForecastSince2004(Ssq_blue_method method) {
		String sql = "select * from ssq_drawing_history where term>2016014 order by term asc";
		List<Ssq_drawing_history> list = Ssq_drawing_history.me.find(sql);
		int term = 0;
		//预测所有已开奖的期次
		for(int i=0;i<list.size();i++){
			Ssq_drawing_history history = list.get(i);
			term = history.getItem(0);
			forecast(term, method);
			BlueMethodAnalysisService.analysis(history, method);
		}
	}

	/**
	 * 预测term期次的中奖红球号码
	 * @param term
	 * @param method
	 */
	public static void forecast(int term, Ssq_blue_method method) {
		Ssq_blue_method_forecast forecast = new Ssq_blue_method_forecast();
		
		String javaClass = method.getStr("javaClass");
		int methodId = method.getInt("id");
		
		forecast.set("term", term);
		forecast.set("methodId", methodId);
		forecast.set("type", method.getStr("type"));
		try {
			Forecast f = (Forecast) Class.forName(javaClass).newInstance();
			List<Integer> forecastList = f.forecast(term);
			String blues = BallUtil.toString(forecastList);
			forecast.set("blues", blues);
			
			String sql = "select * from Ssq_blue_method_analysis where totalNum>0 and methodId=? and term<? order by term desc limit 100";
			List<Ssq_blue_method_analysis> list = Ssq_blue_method_analysis.me.find(sql, methodId, term);
			if(list!=null && list.size()>=0){
				int total = list.size();
				int sucTimes = 0;
				int sucBalls = 0;
				for(int i=0;i<list.size();i++){
					Ssq_blue_method_analysis analysis = list.get(i);
					sucTimes += analysis.getInt("succeed");
					sucBalls += analysis.getInt("sucNum");
				}
				if(sucTimes==0){
					forecast.set("rate", 0.0f);
					forecast.set("confidence", 0.0f);
				}else{
					float rate = sucTimes*1.0f/total * 100;
					float confidence = (sucBalls*1.0f)/sucTimes;
					forecast.set("rate", rate);
					forecast.set("confidence", confidence);
				}
			}
			
			forecast.save();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
