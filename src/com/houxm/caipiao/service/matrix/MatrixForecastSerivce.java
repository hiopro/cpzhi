package com.houxm.caipiao.service.matrix;

import java.util.List;

import com.houxm.caipiao.algorithm.matrix.RotationMatrix;
import com.houxm.caipiao.model.Ssq_matrix_data;
import com.houxm.caipiao.model.Ssq_matrix_method;
import com.houxm.caipiao.model.Ssq_matrix_method_forecast;
import com.houxm.caipiao.model.Ssq_red_plan_forecast;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class MatrixForecastSerivce {

	public static void main(String[] args) {
		PropKit.use("a_little_config.txt");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"),
		PropKit.get("user"),
		PropKit.get("password").trim());
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.addMapping("ssq_red_plan_forecast", Ssq_red_plan_forecast.class);
		arp.addMapping("ssq_matrix_method", Ssq_matrix_method.class);
		arp.addMapping("ssq_matrix_method_forecast", Ssq_matrix_method_forecast.class);
		arp.addMapping("ssq_matrix_data", Ssq_matrix_data.class);
		
		arp.start();
		
		List<Ssq_matrix_method> list = Ssq_matrix_method.me.find("select * from ssq_matrix_method where id=1");
		for(Ssq_matrix_method method:list)
			initMatrixForecastSince2004(method);
		System.out.println("done");
	}

	private static void initMatrixForecastSince2004(Ssq_matrix_method method) {
		String sql = "select * from ssq_red_plan_forecast where term>2004000 order by term desc";
		List<Ssq_red_plan_forecast> list = Ssq_red_plan_forecast.me.find(sql);
		for(Ssq_red_plan_forecast fore : list){
			rotation(fore, method);
		}
	}
	
	public static List<Ssq_matrix_method_forecast> rotation(Ssq_red_plan_forecast fore, Ssq_matrix_method method){
		return rotation(fore, 15, method);
	}
	
	public static List<Ssq_matrix_method_forecast> rotation(Ssq_red_plan_forecast fore, int redNum, Ssq_matrix_method method){
		List<Ssq_matrix_method_forecast> list = null;
		int[] reds = fore.getReds(redNum);
		int term = fore.getInt("term");
		String javaClass = method.getStr("javaClass");
		try {
			RotationMatrix rm = (RotationMatrix) Class.forName(javaClass).newInstance();
			list = rm.rotation(reds, term);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return list;
	}

}
