package com.houxm.caipiao.service;

import java.util.List;

import com.houxm.caipiao.model.Ssq_blue_plan_analysis;
import com.houxm.caipiao.model.Ssq_red_plan_analysis;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class SetData {

	public static void main(String[] args) {
		PropKit.use("a_little_config.txt");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"),
		PropKit.get("user"),
		PropKit.get("password").trim());
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.addMapping("ssq_blue_plan_analysis", Ssq_blue_plan_analysis.class);
		arp.addMapping("ssq_red_plan_analysis", Ssq_red_plan_analysis.class);
		
		arp.start();
		
		set();
	}
	
	public static void set(){
		String sql = "select * from ssq_red_plan_analysis order by term desc";
		List<Ssq_red_plan_analysis> list = Ssq_red_plan_analysis.me.find(sql);
		for(Ssq_red_plan_analysis analy : list){
			String suc = analy.getStr("sucIndex");
			String[] s = suc.split(",");
			int sucNum = 0;
			for(String tmp:s){
				if(Integer.parseInt(tmp)<16){
					sucNum++;
				}else{
					break;
				}
			}
			analy.set("sucNum15", sucNum);
			analy.update();
		}
		
		String sql2 = "select * from Ssq_blue_plan_analysis order by term desc";
		List<Ssq_blue_plan_analysis> list2 = Ssq_blue_plan_analysis.me.find(sql2);
		for(Ssq_blue_plan_analysis analy : list2){
			int suc = analy.getInt("factIndex");
			int sucNum = 0;
			if(suc<7){
				sucNum++;
			}
			analy.set("sucNum6", sucNum);
			analy.update();
		}
		
		String sql3 = "select * from ssq_red_plan_analysis order by term asc";
		List<Ssq_red_plan_analysis> list3 = Ssq_red_plan_analysis.me.find(sql3);
		for(Ssq_red_plan_analysis analy : list3){
			int term = analy.getInt("term");
			String sql100 = "select * from ssq_red_plan_analysis where term<=? order by term desc limit 100";
			List<Ssq_red_plan_analysis> list100 = Ssq_red_plan_analysis.me.find(sql100, term);
			float avg100=0.0f,avg10=0.0f;
			int total10 = 0 , total100 = 0;
			int totalNum10 = 0 , totalNum100 = 0;
			for(int i=0;i<list100.size();i++){
				Ssq_red_plan_analysis a = list100.get(i);
				if(i<10){
					totalNum10++;
					total10+=a.getInt("sucNum15");
				}
				totalNum100++;
				total100+=a.getInt("sucNum15");
			}
			avg10 = total10*1.0f/totalNum10;
			avg100 = total100*1.0f/totalNum100;
			analy.set("suc10", avg10);
			analy.set("suc100", avg100);
			analy.update();
		}
		
		String sql4 = "select * from Ssq_blue_plan_analysis order by term asc";
		List<Ssq_blue_plan_analysis> list4 = Ssq_blue_plan_analysis.me.find(sql4);
		for(Ssq_blue_plan_analysis analy : list4){
			int term = analy.getInt("term");
			String sql100 = "select * from Ssq_blue_plan_analysis where term<=? order by term desc limit 100";
			List<Ssq_blue_plan_analysis> list100 = Ssq_blue_plan_analysis.me.find(sql100, term);
			int total10 = 0 , total100 = 0;
			for(int i=0;i<list100.size();i++){
				Ssq_blue_plan_analysis a = list100.get(i);
				if(i<10){
					total10+=a.getInt("sucNum6");
				}
				total100+=a.getInt("sucNum6");
			}
			analy.set("suc10", total10);
			analy.set("suc100", total100);
			analy.update();
		}
	}

}
