package com.houxm.caipiao.service;

import java.util.List;

import com.houxm.caipiao.model.Ssq_blue_method;
import com.houxm.caipiao.model.Ssq_blue_method_analysis;
import com.houxm.caipiao.model.Ssq_blue_method_forecast;
import com.houxm.caipiao.model.Ssq_blue_plan;
import com.houxm.caipiao.model.Ssq_blue_plan_analysis;
import com.houxm.caipiao.model.Ssq_blue_plan_forecast;
import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_drawing_reward;
import com.houxm.caipiao.model.Ssq_filter_method;
import com.houxm.caipiao.model.Ssq_group_plan;
import com.houxm.caipiao.model.Ssq_group_plan_forecast;
import com.houxm.caipiao.model.Ssq_matrix_data;
import com.houxm.caipiao.model.Ssq_matrix_method;
import com.houxm.caipiao.model.Ssq_matrix_method_forecast;
import com.houxm.caipiao.model.Ssq_red_method;
import com.houxm.caipiao.model.Ssq_red_method_analysis;
import com.houxm.caipiao.model.Ssq_red_method_forecast;
import com.houxm.caipiao.model.Ssq_red_plan;
import com.houxm.caipiao.model.Ssq_red_plan_analysis;
import com.houxm.caipiao.model.Ssq_red_plan_forecast;
import com.houxm.caipiao.service.blue.BlueMethodAnalysisService;
import com.houxm.caipiao.service.blue.BlueMethodForecastService;
import com.houxm.caipiao.service.blue.BluePlanAnalysisService;
import com.houxm.caipiao.service.blue.BluePlanForecastService;
import com.houxm.caipiao.service.group.GroupPlanAnalysis;
import com.houxm.caipiao.service.group.GroupPlanForecast;
import com.houxm.caipiao.service.matrix.MatrixForecastSerivce;
import com.houxm.caipiao.service.red.RedMethodAnalysisService;
import com.houxm.caipiao.service.red.RedMethodForecastService;
import com.houxm.caipiao.service.red.RedPlanAnalysisService;
import com.houxm.caipiao.service.red.RedPlanForecastService;
import com.houxm.caipiao.service.spider.SsqDrawing163Util;
import com.houxm.caipiao.service.spider.SsqDrawing500Util;
import com.houxm.caipiao.service.spider.SsqDrawingZhcwUtil;
import com.houxm.caipiao.util.DrawingHistoryUtil;
import com.houxm.caipiao.util.NextTermUtil;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class LoadNextService {

	public static void main(String[] args) {
		PropKit.use("a_little_config.txt");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"),
		PropKit.get("user"),
		PropKit.get("password").trim());
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.addMapping("ssq_drawing_reward", Ssq_drawing_reward.class);
		arp.addMapping("ssq_drawing_history", Ssq_drawing_history.class);
		arp.addMapping("ssq_red_method", Ssq_red_method.class);
		arp.addMapping("ssq_red_method_forecast", Ssq_red_method_forecast.class);
		arp.addMapping("ssq_red_method_analysis", Ssq_red_method_analysis.class);
		arp.addMapping("ssq_red_plan", Ssq_red_plan.class);
		arp.addMapping("ssq_red_plan_forecast", Ssq_red_plan_forecast.class);
		arp.addMapping("ssq_red_plan_analysis", Ssq_red_plan_analysis.class);
		arp.addMapping("ssq_blue_method", Ssq_blue_method.class);
		arp.addMapping("ssq_blue_method_forecast", Ssq_blue_method_forecast.class);
		arp.addMapping("ssq_blue_method_analysis", Ssq_blue_method_analysis.class);
		arp.addMapping("ssq_blue_plan", Ssq_blue_plan.class);
		arp.addMapping("ssq_blue_plan_forecast", Ssq_blue_plan_forecast.class);
		arp.addMapping("ssq_blue_plan_analysis", Ssq_blue_plan_analysis.class);
		arp.addMapping("ssq_matrix_method", Ssq_matrix_method.class);
		arp.addMapping("ssq_matrix_data", Ssq_matrix_data.class);
		arp.addMapping("ssq_matrix_method_forecast", Ssq_matrix_method_forecast.class);
		arp.addMapping("ssq_group_plan", Ssq_group_plan.class);
		arp.addMapping("ssq_filter_method", Ssq_filter_method.class);
		arp.addMapping("ssq_group_plan_forecast", Ssq_group_plan_forecast.class);
		arp.start();
		forecastAll(2017001);
//		boolean result = loadNext();
//		System.out.println(result);
//		System.out.println(NextTermUtil.next());
		arp.stop();
	}
	
	/**
	 * 分析下一期次双色球的期次号码，然后从中彩网下载当前期次的中奖号码。
	 * 
	 * @return
	 */
	public static boolean loadNext(){
		boolean flag = false;
		int nextTerm = NextTermUtil.next();
		int nextNext = NextTermUtil.nextNext();
		
		flag = SsqDrawing163Util.spiderNext(nextTerm);
		
		if(!flag)
			flag = SsqDrawingZhcwUtil.spiderNext(nextTerm);
		
		if(!flag)
			flag = SsqDrawing500Util.spiderNext(nextTerm);
		
		//获取开奖信息成功，则进行上一期次的开奖数据分析
		//并且预测下一次期的开奖号码
		if(flag){
			analysisAll(nextTerm);
			forecastAll(nextNext);
		}
		return flag;
	}
	
	/**
	 * 对指定期次的预测内容进行开奖分析
	 * @param term
	 */
	public static void analysisAll(int term){
		
		Ssq_drawing_history history = DrawingHistoryUtil.get(term);
		//根据当前期次开奖结果，计算分析各红球预测方法的预测结果
		List<Ssq_red_method> redMethod = Ssq_red_method.me.find("select * from ssq_red_method");
		for(Ssq_red_method method:redMethod){
			RedMethodAnalysisService.analysis(history, method);
		}
		//根据当前期次开奖结果，计算分析官网推荐的红球预测方案的预测结果
		Ssq_red_plan redPlan = Ssq_red_plan.me.findFirst("SELECT * FROM ssq_red_plan where id=1");
		RedPlanAnalysisService.analysis(history, redPlan);
		//根据当前期次开奖结果，计算分析各蓝球预测方法的预测结果
		List<Ssq_blue_method> blueMethod = Ssq_blue_method.me.find("select * from ssq_blue_method");
		for(Ssq_blue_method method:blueMethod)
			BlueMethodAnalysisService.analysis(history, method);
		//根据当前期次开奖结果，计算分析官网推荐的蓝球预测方案的预测结果
		Ssq_blue_plan bluePlan = Ssq_blue_plan.me.findFirst("SELECT * FROM ssq_blue_plan where id=1");
		BluePlanAnalysisService.analysis(history, bluePlan);
		
		String groupPlanSql = "select * from ssq_group_plan where id=1";
		Ssq_group_plan groupPlan = Ssq_group_plan.me.findFirst(groupPlanSql);
		GroupPlanAnalysis.analysis(history, groupPlan);
	}
	
	/**
	 * 对指定期次进行预测
	 * @param term
	 */
	public static void forecastAll(int term){
		//使用所有红球的预测方法计算红球出球的积分
		List<Ssq_red_method> redMethod = Ssq_red_method.me.find("select * from ssq_red_method");
		for(Ssq_red_method method:redMethod)
			RedMethodForecastService.forecast(term, method);
		//按照官网推荐的预测方案，将所有红球预测的结果的积分进行合并计算，积分小于61.8分的不计入计算范围，最终得出红球积分排序
		Ssq_red_plan redPlan = Ssq_red_plan.me.findFirst("SELECT * FROM ssq_red_plan where id=1");
		RedPlanForecastService.forecastByGoldenSection(term, redPlan);
		//使用所有蓝球的预测方法计算蓝球出球的积分
		List<Ssq_blue_method> blueMethod = Ssq_blue_method.me.find("select * from ssq_blue_method");
		for(Ssq_blue_method method:blueMethod)
			BlueMethodForecastService.forecast(term, method);
		//按照官网推荐的预测方案，将所有蓝球预测的结果的积分进行合并计算，积分小于38.2分的不计入计算范围，最终得出蓝球积分排序
		Ssq_blue_plan bluePlan = Ssq_blue_plan.me.findFirst("SELECT * FROM ssq_blue_plan where id=1");
		BluePlanForecastService.forecastByGoldenSection(term, bluePlan);
		
		List<Ssq_matrix_method_forecast> matrix = null;
		List<Ssq_matrix_method> list = Ssq_matrix_method.me.find("select * from ssq_matrix_method where id=1");
		
		String sql = "select * from ssq_red_plan_forecast where term=?";
		Ssq_red_plan_forecast fore = Ssq_red_plan_forecast.me.findFirst(sql, term);
		for(Ssq_matrix_method method:list){
			matrix = MatrixForecastSerivce.rotation(fore, method);
		}
		String groupPlanSql = "select * from ssq_group_plan where id=1";
		Ssq_group_plan groupPlan = Ssq_group_plan.me.findFirst(groupPlanSql);
		matrix = GroupPlanForecast.forecast(matrix, term, groupPlan);
		
		Ssq_blue_plan_forecast bluePlanFore = Ssq_blue_plan_forecast.me.findFirst("select * from ssq_blue_plan_forecast where term=?", term);
		String blues = bluePlanFore.getBlues(5);
		GroupPlanForecast.saveToGroupPlanForecast(matrix, blues, groupPlan);
	}

}
