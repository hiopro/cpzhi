package com.houxm.caipiao.service.red;

import java.util.List;

import com.houxm.caipiao.common.Constant;
import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_red_plan;
import com.houxm.caipiao.model.Ssq_red_plan_analysis;
import com.houxm.caipiao.model.Ssq_red_plan_forecast;

public class RedPlanAnalysisService {

	/**
	 * 对某一期次的某一计划的红球出球排序进行分析统计出球范围。
	 * @param history
	 * @param plan
	 */
	public static void analysis(Ssq_drawing_history history, Ssq_red_plan plan) {
		String sql = "select * from ssq_red_plan_forecast where term=? and planId=?";
		int term = history.getItem(0);
		int planId = plan.getInt("id");
		System.out.println("term: "+term+"\t\tplanId: "+planId);
		Ssq_red_plan_forecast fore = Ssq_red_plan_forecast.me.findFirst(sql, term, planId);
		String reds = fore.getStr("reds");
		String[] red = reds.split(",");
		int start=0,end=0,sucNum15=0;
		String sucIndex = "";
		for(int i=0;i<red.length;i++){
			if(history.isWinningRed(Integer.parseInt(red[i]))){
				if(start==0)
					start = i+1;
				
				end = i+1;
				sucIndex += (i+1)+",";
				if(i<Constant.RED_RECOMMEND_INDEX){
					sucNum15++;
				}
			}
		}
		sucIndex = sucIndex.substring(0,sucIndex.length()-1);
		
		Ssq_red_plan_analysis analysis = new Ssq_red_plan_analysis();
		analysis.set("term", term);
		analysis.set("planId", planId);
		analysis.set("startNum", start);
		analysis.set("endNum", end);
		analysis.set("sucIndex", sucIndex);
		analysis.set("sucNum15", sucNum15);
		
		String above100 = fore.showRedsAbove100();
		if(above100!=null && !"".equals(above100)){
			String[] above = above100.split(",");
			analysis.set("desiredEndNum", above.length);
			int sucNum = 0;
			for(int i=0;i<above.length;i++){
				if(history.isWinningRed(Integer.parseInt(above[i]))){
					sucNum++;
				}
			}
			
			int succeed = 0;
			float thisSucRate = 0.0f;
			if(sucNum==6)
				succeed = 1;
			thisSucRate = sucNum*1.0f/6;
			
			analysis.set("sucNum", sucNum);
			analysis.set("succeed", succeed);
			analysis.set("thisSucRate", thisSucRate);
		}else{
			analysis.set("desiredEndNum", 0);
			analysis.set("sucNum", 0);
			analysis.set("succeed", 0);
			analysis.set("thisSucRate", 0.0f);
		}
		
		String avgSql = "select * from ssq_red_plan_analysis where planId=? and term<=? and succeed>=0 order by term desc limit 100";
		List<Ssq_red_plan_analysis> list = Ssq_red_plan_analysis.me.find(avgSql, planId, term);
		if(list==null || list.size()==0){
			analysis.set("avgRate", analysis.getFloat("thisSucRate"));
			analysis.set("avgSucNum", analysis.getInt("sucNum"));
			analysis.set("succeedRate", analysis.getInt("succeed")*1.0f);
		}else{
			int total = list.size();
			int sucTotal = 0, suc15Total100 = 0, suc15Total10 = 0;
			float rateTotal = 0.0f;
			int succeedTotal = 0;
			for(int i=0;i<total;i++){
				Ssq_red_plan_analysis a = list.get(i);
				sucTotal += a.getInt("sucNum");
				rateTotal += a.getFloat("thisSucRate");
				succeedTotal += a.getInt("succeed");
				suc15Total100 += a.getInt("sucNum15");
				if(i<10-1){
					suc15Total10 += a.getInt("sucNum15");
				}
			}
			
			//本条要保存的结果分析也加入计算
			sucTotal += analysis.getInt("sucNum");
			rateTotal += analysis.getFloat("thisSucRate");
			succeedTotal += analysis.getInt("succeed");
			suc15Total10 += analysis.getInt("sucNum15");
			suc15Total100 += analysis.getInt("sucNum15");
			total++;
			
			analysis.set("avgRate", rateTotal/total);
			analysis.set("avgSucNum", sucTotal*1.0f/total);
			analysis.set("succeedRate", succeedTotal*1.0f/total);
			analysis.set("suc100", suc15Total100*1.0f/total);
			analysis.set("suc10", suc15Total10*1.0f/(total>9?10:total));
		}
		
		analysis.save();
	}

}
