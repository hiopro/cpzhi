package com.houxm.caipiao.service.group;

import java.util.List;

import com.houxm.caipiao.model.Ssq_blue_plan_forecast;
import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_drawing_reward;
import com.houxm.caipiao.model.Ssq_filter_method;
import com.houxm.caipiao.model.Ssq_group_plan;
import com.houxm.caipiao.model.Ssq_group_plan_analysis;
import com.houxm.caipiao.model.Ssq_group_plan_forecast;
import com.houxm.caipiao.model.Ssq_matrix_data;
import com.houxm.caipiao.model.Ssq_matrix_method;
import com.houxm.caipiao.model.Ssq_matrix_method_forecast;
import com.houxm.caipiao.model.Ssq_red_plan;
import com.houxm.caipiao.model.Ssq_red_plan_analysis;
import com.houxm.caipiao.model.Ssq_red_plan_forecast;
import com.houxm.caipiao.service.matrix.MatrixForecastSerivce;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class GroupPlanForecast {

	public static void main(String[] args) {
		PropKit.use("a_little_config.txt");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"),
		PropKit.get("user"),
		PropKit.get("password").trim());
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.addMapping("ssq_red_plan_forecast", Ssq_red_plan_forecast.class);
		arp.addMapping("ssq_matrix_method", Ssq_matrix_method.class);
		arp.addMapping("ssq_matrix_method_forecast", Ssq_matrix_method_forecast.class);
		arp.addMapping("ssq_matrix_data", Ssq_matrix_data.class);
		
		arp.addMapping("ssq_red_plan", Ssq_red_plan.class);
		arp.addMapping("ssq_red_plan_analysis", Ssq_red_plan_analysis.class);
		arp.addMapping("ssq_blue_plan_forecast", Ssq_blue_plan_forecast.class);
		arp.addMapping("ssq_drawing_reward", Ssq_drawing_reward.class);
		arp.addMapping("ssq_drawing_history", Ssq_drawing_history.class);
		arp.addMapping("ssq_filter_method", Ssq_filter_method.class);
		arp.addMapping("ssq_group_plan_analysis", Ssq_group_plan_analysis.class);
		arp.addMapping("ssq_group_plan_forecast", Ssq_group_plan_forecast.class);
		arp.addMapping("ssq_group_plan", Ssq_group_plan.class);
		
		arp.start();
		
		String sql = "select * from ssq_red_plan_forecast where term>2004000 order by term asc";
		List<Ssq_red_plan_forecast> list2 = Ssq_red_plan_forecast.me.find(sql);
		for(Ssq_red_plan_forecast fore : list2){
			List<Ssq_matrix_method_forecast> matrix = null;
			List<Ssq_matrix_method> list = Ssq_matrix_method.me.find("select * from ssq_matrix_method where id=1");
			for(Ssq_matrix_method method:list){
				matrix = MatrixForecastSerivce.rotation(fore, method);
			}
			
			String groupPlanSql = "select * from ssq_group_plan where id=1";
			Ssq_group_plan plan = Ssq_group_plan.me.findFirst(groupPlanSql);
			
			int term = fore.getInt("term");
			matrix = forecast(matrix, term, plan);
			
			Ssq_blue_plan_forecast bluePlan = Ssq_blue_plan_forecast.me.findFirst("select * from ssq_blue_plan_forecast where term=?", term);
			String blues = bluePlan.getBlues(5);
			saveToGroupPlanForecast(matrix, blues, plan);
			
			if(term==2016016)
				break;
			String drawingSql = "SELECT * FROM ssq_drawing_history where term=?";
			Ssq_drawing_history history = Ssq_drawing_history.me.findFirst(drawingSql, term);
			GroupPlanAnalysis.analysis(history, plan);
			
		}
		
		
		System.out.println("done");
	}
	
	/**
	 * 根据组合过滤方案，将矩阵方法预测的结果进行过滤，并返回
	 * @param matrix
	 * @param term
	 * @param plan
	 * @return
	 */
	public static List<Ssq_matrix_method_forecast> forecast(List<Ssq_matrix_method_forecast> matrix, int term, Ssq_group_plan plan){
		String filterMethodIds = plan.getStr("filterMethodIds");
		String[] filterMethodId = filterMethodIds.split(",");
		for(String id : filterMethodId){
			String filterSql = "select * from ssq_filter_method where id=?";
			Ssq_filter_method m = Ssq_filter_method.me.findFirst(filterSql, Integer.parseInt(id));
			matrix = FilterExcludeService.exclude(matrix, term, m);
//			for(Ssq_matrix_method_forecast f:matrix)
//				System.out.print(f.getInt("id")+ " ");
//			System.out.println();
		}
		return matrix;
	}

	/**
	 * 保存彩票预测组合
	 * @param matrix
	 * @param blues
	 */
	public static void saveToGroupPlanForecast(
			List<Ssq_matrix_method_forecast> matrix, String blues, Ssq_group_plan plan) {
		matrix = sortGroup(matrix);
		for(Ssq_matrix_method_forecast fore:matrix){
			Ssq_group_plan_forecast group = new Ssq_group_plan_forecast();
			group.set("term", fore.getTerm());
			group.set("red1", fore.getBall(1));
			group.set("red2", fore.getBall(2));
			group.set("red3", fore.getBall(3));
			group.set("red4", fore.getBall(4));
			group.set("red5", fore.getBall(5));
			group.set("red6", fore.getBall(6));
			group.set("blues", blues);
			group.set("planId", plan.getInt("id"));
			group.save();
		}
	}

	private static List<Ssq_matrix_method_forecast> sortGroup(
			List<Ssq_matrix_method_forecast> matrix) {
		for(int i=0;i<matrix.size()-1;i++){
			for (int j = i+1; j < matrix.size(); j++) {
				for (int k = 1; k < 7; k++) {
					if(matrix.get(i).getBall(k)>matrix.get(j).getBall(k)){
						Ssq_matrix_method_forecast tmp = matrix.get(i);
						matrix.set(i, matrix.get(j));
						matrix.set(j, tmp);
					}
				}
			}
		}
		return matrix;
	}

}
