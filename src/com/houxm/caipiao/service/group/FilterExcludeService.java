package com.houxm.caipiao.service.group;

import java.util.List;

import com.houxm.caipiao.algorithm.filter.Exclude;
import com.houxm.caipiao.model.Ssq_filter_method;
import com.houxm.caipiao.model.Ssq_matrix_method_forecast;

public class FilterExcludeService {

	public static List<Ssq_matrix_method_forecast> exclude(
			List<Ssq_matrix_method_forecast> matrix, Integer term,
			Ssq_filter_method m) {
		String javaClass = m.getStr("javaClass");
		try {
			Exclude ex = (Exclude) Class.forName(javaClass).newInstance();
			matrix = ex.exclude(matrix, term);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return matrix;
	}

}
