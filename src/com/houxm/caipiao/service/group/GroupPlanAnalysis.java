package com.houxm.caipiao.service.group;

import java.util.List;

import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_drawing_reward;
import com.houxm.caipiao.model.Ssq_group_plan;
import com.houxm.caipiao.model.Ssq_group_plan_analysis;
import com.houxm.caipiao.model.Ssq_group_plan_forecast;

public class GroupPlanAnalysis {

	public static void analysis(Ssq_drawing_history history, Ssq_group_plan plan){
		int term = history.getItem(0);
		int planId = plan.getInt("id");
		String sql = "select * from ssq_group_plan_forecast where term=? and planId=?";
		List<Ssq_group_plan_forecast> list = Ssq_group_plan_forecast.me.find(sql, term, planId);
		
		int lvl1=0,lvl2=0,lvl3=0,lvl4=0,lvl5=0,lvl6=0;
		for(Ssq_group_plan_forecast fore:list){
			String blues = fore.getStr("blues");
			String[] blue = blues.split(",");
			int redNum = 0, blueNum = 0;
			for(String blueStr:blue){
				if(history.isWinningBlue(Integer.parseInt(blueStr))){
					blueNum++;
				}
			}
			
			for(int i=1;i<7;i++){
				if(history.isWinningRed(fore.getBall(i))){
					redNum++;
				}
			}
			
			switch(redNum){
			case 6:
				if(blueNum==1){
					lvl1++;
				}else{
					lvl2++;
				}
				break;
			case 5:
				if(blueNum==1){
					lvl3++;
				}else{
					lvl4++;
				}
				break;
			case 4:
				if(blueNum==1){
					lvl4++;
				}else{
					lvl5++;
				}
				break;
			case 3:
				if(blueNum==1){
					lvl5++;
				}else{
					lvl6++;
				}
				break;
			default:
				if(blueNum==1){
					lvl6++;
				}
			}
		}
		
		String rewardSql = "SELECT * FROM ssq_drawing_reward where term=?";
		Ssq_drawing_reward reward = Ssq_drawing_reward.me.findFirst(rewardSql, term);
		float winMoney = 0.0f;
		winMoney = lvl1*reward.getFloat("lvl1money") + lvl2*reward.getFloat("lvl2money")
				+lvl3*3000 + lvl4*200 + lvl5*10 + lvl6*5;
		
		Ssq_group_plan_analysis analysis = new Ssq_group_plan_analysis();
		analysis.set("term", term);
		analysis.set("planId", planId);
		analysis.set("lvl1", lvl1);
		analysis.set("lvl2", lvl2);
		analysis.set("lvl3", lvl3);
		analysis.set("lvl4", lvl4);
		analysis.set("lvl5", lvl5);
		analysis.set("lvl6", lvl6);
		analysis.set("winMoney", winMoney);
		analysis.save();
	}
}
