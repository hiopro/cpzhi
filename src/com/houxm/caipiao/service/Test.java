package com.houxm.caipiao.service;

import java.util.List;

import com.houxm.caipiao.common.Constant;
import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_filter_method;
import com.houxm.caipiao.model.Ssq_group_plan;
import com.houxm.caipiao.model.Ssq_matrix_method_forecast;
import com.houxm.caipiao.model.Ssq_red_plan;
import com.houxm.caipiao.model.Ssq_red_plan_analysis;
import com.houxm.caipiao.model.Ssq_red_plan_forecast;
import com.houxm.caipiao.service.group.GroupPlanForecast;
import com.houxm.caipiao.util.DrawingHistoryUtil;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class Test {

	public static void main(String[] args) {
		PropKit.use("a_little_config.txt");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"),
		PropKit.get("user"),
		PropKit.get("password").trim());
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.addMapping("ssq_matrix_method_forecast", Ssq_matrix_method_forecast.class);
		arp.addMapping("ssq_drawing_history", Ssq_drawing_history.class);
		arp.addMapping("ssq_red_plan", Ssq_red_plan.class);
		arp.addMapping("ssq_red_plan_forecast", Ssq_red_plan_forecast.class);
		arp.addMapping("ssq_red_plan_analysis", Ssq_red_plan_analysis.class);
		arp.addMapping("ssq_group_plan", Ssq_group_plan.class);
		arp.addMapping("ssq_filter_method", Ssq_filter_method.class);
		
		arp.start();
		
		/*String sql = "SELECT * FROM ssq_matrix_method_forecast where term=2016009";
//		String sql2 = "SELECT * FROM ssq_drawing_history where term=2016009";
		
//		Ssq_drawing_history his = Ssq_drawing_history.me.findFirst(sql2);
		List<Ssq_matrix_method_forecast> fores = Ssq_matrix_method_forecast.me.find(sql);
		
		String groupPlanSql = "select * from ssq_group_plan where id=1";
		Ssq_group_plan plan = Ssq_group_plan.me.findFirst(groupPlanSql);
		
		int term = 2016009;
		GroupPlanForecast.forecast(fores, term, plan);*/
		
		int term = 2016048;
		Ssq_drawing_history history = DrawingHistoryUtil.get(term);
		Ssq_red_plan redPlan = Ssq_red_plan.me.findFirst("SELECT * FROM ssq_red_plan where id=1");
		analysis(history, redPlan);
	}
	
	public static void analysis(Ssq_drawing_history history, Ssq_red_plan plan) {
		String sql = "select * from ssq_red_plan_forecast where term=? and planId=?";
		int term = history.getItem(0);
		int planId = plan.getInt("id");
		Ssq_red_plan_forecast fore = Ssq_red_plan_forecast.me.findFirst(sql, term, planId);
		String reds = fore.getStr("reds");
		String[] red = reds.split(",");
		int start=0,end=0,sucNum15=0;
		String sucIndex = "";
		for(int i=0;i<red.length;i++){
			if(history.isWinningRed(Integer.parseInt(red[i]))){
				if(start==0)
					start = i+1;
				
				end = i+1;
				sucIndex += (i+1)+",";
				if(i<Constant.RED_RECOMMEND_INDEX){
					sucNum15++;
				}
			}
		}
		sucIndex = sucIndex.substring(0,sucIndex.length()-1);
		
		Ssq_red_plan_analysis analysis = new Ssq_red_plan_analysis();
		analysis.set("term", term);
		analysis.set("planId", planId);
		analysis.set("startNum", start);
		analysis.set("endNum", end);
		analysis.set("sucIndex", sucIndex);
		analysis.set("sucNum15", sucNum15);
		
		String above100 = fore.showRedsAbove100();
		if(above100!=null && !"".equals(above100)){
			String[] above = above100.split(",");
			analysis.set("desiredEndNum", above.length);
			int sucNum = 0;
			for(int i=0;i<above.length;i++){
				if(history.isWinningRed(Integer.parseInt(above[i]))){
					sucNum++;
				}
			}
			
			int succeed = 0;
			float thisSucRate = 0.0f;
			if(sucNum==6)
				succeed = 1;
			thisSucRate = sucNum*1.0f/6;
			
			analysis.set("sucNum", sucNum);
			analysis.set("succeed", succeed);
			analysis.set("thisSucRate", thisSucRate);
		}else{
			analysis.set("desiredEndNum", 0);
			analysis.set("sucNum", 0);
			analysis.set("succeed", 0);
			analysis.set("thisSucRate", 0.0f);
		}
		
		String avgSql = "select * from ssq_red_plan_analysis where planId=? and term<=? and succeed>=0 order by term desc limit 100";
		List<Ssq_red_plan_analysis> list = Ssq_red_plan_analysis.me.find(avgSql, planId, term);
		if(list==null || list.size()==0){
			analysis.set("avgRate", analysis.getFloat("thisSucRate"));
			analysis.set("avgSucNum", analysis.getInt("sucNum"));
			analysis.set("succeedRate", analysis.getInt("succeed")*1.0f);
		}else{
			int total = list.size();
			int sucTotal = 0, suc15Total100 = 0, suc15Total10 = 0;
			float rateTotal = 0.0f;
			int succeedTotal = 0;
			for(int i=0;i<total;i++){
				Ssq_red_plan_analysis a = list.get(i);
				sucTotal += a.getInt("sucNum");
				rateTotal += a.getFloat("thisSucRate");
				succeedTotal += a.getInt("succeed");
				suc15Total100 += a.getInt("sucNum15");
				if(i<10-1){
					suc15Total10 += a.getInt("sucNum15");
				}
			}
			
			//本条要保存的结果分析也加入计算
			sucTotal += analysis.getInt("sucNum");
			rateTotal += analysis.getFloat("thisSucRate");
			succeedTotal += analysis.getInt("succeed");
			suc15Total10 += analysis.getInt("sucNum15");
			suc15Total100 += analysis.getInt("sucNum15");
			total++;
			
			analysis.set("avgRate", rateTotal/total);
			analysis.set("avgSucNum", sucTotal*1.0f/total);
			analysis.set("succeedRate", succeedTotal*1.0f/total);
			analysis.set("suc100", suc15Total100*1.0f/total);
			analysis.set("suc10", suc15Total10*1.0f/(total>9?10:total));
		}
		
//		analysis.save();
	}

}
