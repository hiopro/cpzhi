package com.houxm.caipiao.service.spider;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_drawing_reward;

public class SsqDrawing163Util {

	public static void main(String[] args) throws IOException {
		/**
		 * 获取开奖号球 http://caipiao.163.com/award/ssq/2017092.html
		 */
		String url163 = "http://caipiao.163.com/award/ssq/" + 2017092 + ".html";
		Document doc = Jsoup.connect(url163).get();
		Elements sale = doc.select("#sale");
		System.out.println("sale: " + sale.text());
		Elements pool = doc.select("#pool");
		System.out.println("pool: " + pool.text());
		Elements reds = doc.select("#zj_area .red_ball");
		System.out.println(reds.get(0).text());
		Elements blue = doc.select("#zj_area .blue_ball");
		System.out.println("blue: " + blue.text());
		Elements rewardTable = doc.getElementsByClass("search_zj_right zjxx");
		Document reward = Jsoup.parse(rewardTable.toString());
		Elements tds = reward.select(".table2 tr td");
		for (Element td : tds) {
			System.out.println(td.text());
		}
	}

	/**
	 * 爬取网页中的开奖信息，并记录到数据库中.
	 * 
	 * @param term
	 * @return isSuccess
	 */
	public static boolean spiderNext(int term) {
		boolean flag = false;
		try {
			flag = writeBallsAndReward(term);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@SuppressWarnings("deprecation")
	public static boolean writeBallsAndReward(int term) throws Exception {
		String url163 = "http://caipiao.163.com/award/ssq/" + term + ".html";
		Document doc = Jsoup.connect(url163).get();
		Elements sale = doc.select("#sale");
		Elements pool = doc.select("#pool");
		Elements reds = doc.select("#zj_area .red_ball");
		Elements blue = doc.select("#zj_area .blue_ball");
		
		//验证是否为正确的页面，如果页面正确，红球应该可以正确的转换为int值。
		for (int i = 0; i < 6; i++) {
			Integer.parseInt(reds.get(i).text());
		}

		Ssq_drawing_history dh = new Ssq_drawing_history();
		dh.set("term", term);
		dh.set("red1", reds.get(0).text());
		dh.set("red2", reds.get(1).text());
		dh.set("red3", reds.get(2).text());
		dh.set("red4", reds.get(3).text());
		dh.set("red5", reds.get(4).text());
		dh.set("red6", reds.get(5).text());
		dh.set("blue", blue.text());

		Elements rewardTable = doc.getElementsByClass("search_zj_right zjxx");
		Document reward = Jsoup.parse(rewardTable.toString());
		Elements tds = reward.select(".table2 tr td");

		Ssq_drawing_reward dr = new Ssq_drawing_reward();

		dr.set("term", term);
		dr.set("lvl1", tds.get(1).text());
		dr.set("lvl1money", tds.get(2).text());
		dr.set("lvl2", tds.get(4).text());
		dr.set("lvl2money", tds.get(5).text());
		dr.set("lvl3", tds.get(7).text());
		dr.set("lvl4", tds.get(10).text());
		dr.set("lvl5", tds.get(13).text());
		dr.set("lvl6", tds.get(16).text());

		Elements time = doc.select("#time");
		String time_draw_fixed = time.text();
		SimpleDateFormat format3 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date = format3.parse(time_draw_fixed);
		int week = -1;
		if (date != null) {
			week = date.getDay();
			dr.set("lotteryDate", date);
			if (week == 0) {
				week = 7;
			}
			dr.set("lotteryWeek", week);
		}
		dr.set("saleMoney", sale.text());
		dr.set("poolMoney", pool.text());

		dh.save();
		dr.save();
		return true;
	}

}
