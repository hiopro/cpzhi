package com.houxm.caipiao.service.spider;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_drawing_reward;

/**
 * 网站官方已经不维护了，所以此类作废。
 * @author hxm
 *
 */
public class SsqDrawingLecaiUtil {

	public static void main(String[] args) {
		/**
		 * 获取预测号球
		 * http://baidu.lecai.com/lottery/draw/sorts/ajax_get_stats.php?lottery_type=50&play_type=5002&phase=2016042
		 * 
		 * 获取开奖号球
		 * http://baidu.lecai.com/lottery/draw/ajax_get_detail.php?lottery_type=50&phase=2016042
		 */
	}
	
	/**
	 * 获取该期次的中奖情况的网页内容
	 * @param term
	 * @return
	 * @throws Exception
	 */
	public static String getDataFromHtml(int term) throws Exception {
		String jsonUrl = "http://baidu.lecai.com/lottery/draw/ajax_get_detail.php?lottery_type=50&phase="+term;
		URL url = new URL(jsonUrl);
        InputStream is = url.openStream();
        BufferedReader buffer = new BufferedReader(new InputStreamReader(is));
        String str = "";
        String l = null;
        while((l=buffer.readLine())!=null){
        	str+=l;
        }
		return str;
	}

	@SuppressWarnings("deprecation")
	public static boolean writeBallsAndReward(String content, int nextTerm) throws ParseException {
		JSONObject json = new JSONObject(content);
		int code = json.getInt("code");
		if(code==0){
			JSONObject data = json.getJSONObject("data");
			Ssq_drawing_history dh = new Ssq_drawing_history();
			String termStr = data.getString("phase");
			int term = Integer.parseInt(termStr);
			dh.set("term", term);
			JSONObject result = data.optJSONObject("result");
			if(result==null)
				return false;
			JSONArray result2 = result.getJSONArray("result");
			for(int i=0;i<result2.length();i++){
				JSONObject obj = result2.getJSONObject(i);
				if("red".equals(obj.getString("key"))){
					JSONArray reds = obj.getJSONArray("data");
					int red1 = reds.getInt(0);
					int red2 = reds.getInt(1);
					int red3 = reds.getInt(2);
					int red4 = reds.getInt(3);
					int red5 = reds.getInt(4);
					int red6 = reds.getInt(5);
					dh.set("red1", red1);
					dh.set("red2", red2);
					dh.set("red3", red3);
					dh.set("red4", red4);
					dh.set("red5", red5);
					dh.set("red6", red6);
				}else if("blue".equals(obj.getString("key"))){
					JSONArray blue = obj.getJSONArray("data");
					dh.set("blue", blue.getInt(0));
				}
			}
			
			Ssq_drawing_reward dr = new Ssq_drawing_reward();
			
			dr.set("term", term);
			JSONObject resultDetail = data.getJSONObject("result_detail");
			JSONArray resultDetail2 = resultDetail.getJSONArray("resultDetail");
			for(int i=0;i<resultDetail2.length();i++){
				JSONObject obj = resultDetail2.getJSONObject(i);
				if("prize1".equals(obj.getString("key"))){
					dr.set("lvl1", obj.getString("bet"));
					dr.set("lvl1money", obj.getString("prize"));
				}else if("prize2".equals(obj.getString("key"))){
					dr.set("lvl2", obj.getString("bet"));
					dr.set("lvl2money", obj.getString("prize"));
				}else if("prize3".equals(obj.getString("key"))){
					dr.set("lvl3", obj.getString("bet"));
				}else if("prize4".equals(obj.getString("key"))){
					dr.set("lvl4", obj.getString("bet"));
				}else if("prize5".equals(obj.getString("key"))){
					dr.set("lvl5", obj.getString("bet"));
				}else if("prize6".equals(obj.getString("key"))){
					dr.set("lvl6", obj.getString("bet"));
				}
			}
			String time_draw_fixed = data.getString("time_draw_fixed");
			SimpleDateFormat format3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = format3.parse(time_draw_fixed);
			int week = -1;
			if(date!=null){
				week = date.getDay();
				dr.set("lotteryDate", date);
				if(week==0){
					week = 7;
				}
				dr.set("lotteryWeek", week);
			}
			dr.set("saleMoney", data.getString("sale_amount"));
			dr.set("poolMoney", data.getString("latest_pool_amount_str").replace(",", ""));
			
			dh.save();
			dr.save();
			return true;
		}
		return false;
	}

}
