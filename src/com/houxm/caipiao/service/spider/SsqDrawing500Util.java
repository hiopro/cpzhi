package com.houxm.caipiao.service.spider;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.houxm.caipiao.model.Ssq_drawing_history;
import com.houxm.caipiao.model.Ssq_drawing_reward;

public class SsqDrawing500Util {

	public static void main(String[] args) throws IOException {
		/**
		 * 获取开奖号球 
		 */
		String url500 = "http://kaijiang.500.com/shtml/ssq/" + 17092 + ".shtml";
		Document doc = Jsoup.connect(url500).get();
		Elements rewardTable = doc.getElementsByClass("kj_tablelist02");
		Element rt0 = rewardTable.get(0);
		Element rt1 = rewardTable.get(1);
		Document drawing = Jsoup.parse(rt0.toString());
		Document reward = Jsoup.parse(rt1.toString());
		Elements sale = drawing.select(".cfont1");
		System.out.println("sale: " + sale.get(0).text());
		System.out.println("pool: " + sale.get(1).text());
		Elements reds = drawing.select(".ball_box01 .ball_red");
		System.out.println(reds.get(0).text());
		Elements blue = drawing.select(".ball_box01 .ball_blue");
		System.out.println("blue: " + blue.text());
		Elements tds = reward.select("tr td");
		for (Element td : tds) {
			System.out.println(td.text());
		}
	}

	/**
	 * 爬取网页中的开奖信息，并记录到数据库中.
	 * 
	 * @param term
	 * @return isSuccess
	 */
	public static boolean spiderNext(int term) {
		boolean flag = false;
		try {
			flag = writeBallsAndReward(term);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@SuppressWarnings("deprecation")
	public static boolean writeBallsAndReward(int term) throws Exception {
		int term500 = term - 2000000;
		String url500 = "http://kaijiang.500.com/shtml/ssq/" + term500 + ".shtml";
		Document doc = Jsoup.connect(url500).get();
		Elements rewardTable = doc.getElementsByClass("kj_tablelist02");
		Element rt0 = rewardTable.get(0);
		Element rt1 = rewardTable.get(1);
		Document drawing = Jsoup.parse(rt0.toString());
		Document reward = Jsoup.parse(rt1.toString());
		Elements sale = drawing.select(".cfont1");
		Elements reds = drawing.select(".ball_box01 .ball_red");
		Elements blue = drawing.select(".ball_box01 .ball_blue");
		
		//验证是否为正确的页面，如果页面正确，红球应该可以正确的转换为int值。
		for (int i = 0; i < 6; i++) {
			Integer.parseInt(reds.get(i).text());
		}

		Ssq_drawing_history dh = new Ssq_drawing_history();
		dh.set("term", term);
		dh.set("red1", reds.get(0).text());
		dh.set("red2", reds.get(1).text());
		dh.set("red3", reds.get(2).text());
		dh.set("red4", reds.get(3).text());
		dh.set("red5", reds.get(4).text());
		dh.set("red6", reds.get(5).text());
		dh.set("blue", blue.text());

		Elements tds = reward.select("tr td");

		Ssq_drawing_reward dr = new Ssq_drawing_reward();

		dr.set("term", term);
		dr.set("lvl1", tds.get(5).text());
		dr.set("lvl1money", tds.get(6).text());
		dr.set("lvl2", tds.get(8).text());
		dr.set("lvl2money", tds.get(9).text());
		dr.set("lvl3", tds.get(11).text());
		dr.set("lvl4", tds.get(14).text());
		dr.set("lvl5", tds.get(17).text());
		dr.set("lvl6", tds.get(20).text());

		Elements time = drawing.select(".span_right");
		String time_draw_fixed = time.text();
		time_draw_fixed = time_draw_fixed.substring(0, time_draw_fixed.indexOf("兑奖截止日期")).replace("开奖日期：", "").trim();
		SimpleDateFormat format3 = new SimpleDateFormat("yyyy-MM-dd");
		Date date = format3.parse(time_draw_fixed);
		int week = -1;
		if (date != null) {
			week = date.getDay();
			dr.set("lotteryDate", date);
			if (week == 0) {
				week = 7;
			}
			dr.set("lotteryWeek", week);
		}
		dr.set("saleMoney", sale.get(0).text());
		dr.set("poolMoney", sale.get(1).text());

		dh.save();
		dr.save();
		return true;
	}

}
