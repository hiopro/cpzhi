package com.houxm.caipiao.vo;

import com.houxm.caipiao.model.Ssq_blue_plan_analysis;
import com.houxm.caipiao.model.Ssq_red_plan_analysis;


public class SsqPlanAnalysisVO {

	private int term;
	private int planId;
	private String redSucIndex;
	private int redSucNum15;
	private float redSuc100;
	private float redSuc10;
	
	private int blueFactIndex;
	private int blueSucNum6;
	private float blueSuc100;
	private float blueSuc10;
	public int getTerm() {
		return term;
	}
	public void setTerm(int term) {
		this.term = term;
	}
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public String getRedSucIndex() {
		return redSucIndex;
	}
	public void setRedSucIndex(String redSucIndex) {
		this.redSucIndex = redSucIndex;
	}
	public int getRedSucNum15() {
		return redSucNum15;
	}
	public void setRedSucNum15(int redSucNum15) {
		this.redSucNum15 = redSucNum15;
	}
	public float getRedSuc100() {
		return redSuc100;
	}
	public void setRedSuc100(float redSuc100) {
		this.redSuc100 = redSuc100;
	}
	public float getRedSuc10() {
		return redSuc10;
	}
	public void setRedSuc10(float redSuc10) {
		this.redSuc10 = redSuc10;
	}
	public int getBlueFactIndex() {
		return blueFactIndex;
	}
	public void setBlueFactIndex(int blueFactIndex) {
		this.blueFactIndex = blueFactIndex;
	}
	public int getBlueSucNum6() {
		return blueSucNum6;
	}
	public void setBlueSucNum6(int blueSucNum6) {
		this.blueSucNum6 = blueSucNum6;
	}
	public float getBlueSuc100() {
		return blueSuc100;
	}
	public void setBlueSuc100(float blueSuc100) {
		this.blueSuc100 = blueSuc100;
	}
	public float getBlueSuc10() {
		return blueSuc10;
	}
	public void setBlueSuc10(float blueSuc10) {
		this.blueSuc10 = blueSuc10;
	}
	
	public static SsqPlanAnalysisVO build(Ssq_red_plan_analysis red, Ssq_blue_plan_analysis blue){
		SsqPlanAnalysisVO vo = new SsqPlanAnalysisVO();
		if(red==null || blue==null || !red.getInt("term").equals(blue.getInt("term")))
			return null;
		else{
			vo.setTerm(red.getInt("term"));
			vo.setPlanId(red.getInt("planId"));
			vo.setRedSucIndex(red.getStr("sucIndex"));
			vo.setRedSucNum15(red.getInt("sucNum15"));
			vo.setRedSuc100(red.getFloat("suc100"));
			vo.setRedSuc10(red.getFloat("suc10"));
			vo.setBlueFactIndex(blue.getInt("factIndex"));
			vo.setBlueSucNum6(blue.getInt("sucNum6"));
			vo.setBlueSuc100(blue.getInt("suc100"));
			vo.setBlueSuc10(blue.getInt("suc10"));
		}
		return vo;
	}
}
