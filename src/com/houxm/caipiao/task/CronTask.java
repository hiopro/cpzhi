package com.houxm.caipiao.task;

import com.houxm.caipiao.service.LoadNextService;
import com.jfinal.log.Logger;


public class CronTask implements Runnable {

	private static final Logger log = Logger.getLogger(CronTask.class);
	
	@Override
	public void run() {
		//
		log.info("running task.");
		boolean flag = LoadNextService.loadNext();
		if(!flag){
			log.error("task run error.");
		}
	}

}
